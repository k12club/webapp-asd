import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthmobileComponent } from './authmobile.component';

describe('AuthmobileComponent', () => {
  let component: AuthmobileComponent;
  let fixture: ComponentFixture<AuthmobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthmobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthmobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
