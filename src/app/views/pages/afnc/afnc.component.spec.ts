import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfncComponent } from './afnc.component';

describe('AfncComponent', () => {
  let component: AfncComponent;
  let fixture: ComponentFixture<AfncComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfncComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfncComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
