import { Component, OnInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { faChevronRight, faUsers } from '@fortawesome/free-solid-svg-icons';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import { HomeService } from 'src/app/services/home.service';
import * as scriptjs from 'scriptjs';
import vectorMap from "jvectormap";
declare var $: any;
// import { ChartConfiguration, ChartData } from 'chart.js';
import * as Chart from 'chart.js';
import 'chartjs-plugin-datalabels';

@Component({

  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  @ViewChild('thaiMap', { static: true }) thaiMap: ElementRef;
  public SystemName: string = "MF1";
  firstCopy = false;
  faChevronRight = faChevronRight;
  faUsers = faUsers
  countUser
  loading = false
  year: any = "all"
  map
  province = 'กรุงเทพมหานคร'
  countuserProvince
  code_color: any = {
    "TH-10": "#FFFFFF",
    "TH-12": "#aabfd0",
    "TH-13": "#aabfd0",
    "TH-14": "#aabfd0",
    "TH-19": "#aabfd0",
    "TH-15": "#cac7fc",
    "TH-16": "#cac7fc",
    "TH-17": "#cac7fc",
    "TH-18": "#cac7fc",
    "TH-24": "#60c6f7",
    "TH-25": "#60c6f7",
    "TH-27": "#60c6f7",
    "TH-26": "#60c6f7",
    "TH-11": "#60c6f7",
    "TH-71": "#b8dffc",
    "TH-73": "#b8dffc",
    "TH-70": "#b8dffc",
    "TH-72": "#b8dffc",
    "TH-77": "#57ffff",
    "TH-76": "#57ffff",
    "TH-74": "#57ffff",
    "TH-75": "#57ffff",
    "TH-86": "#bff3dc",
    "TH-84": "#bff3dc",
    "TH-80": "#bff3dc",
    "TH-93": "#bff3dc",
    "TH-85": "#5cf990",
    "TH-82": "#5cf990",
    "TH-83": "#5cf990",
    "TH-81": "#5cf990",
    "TH-92": "#5cf990",
    "TH-90": "#20e921",
    "TH-91": "#20e921",
    "TH-94": "#20e921",
    "TH-95": "#20e921",
    "TH-96": "#20e921",
    "TH-22": "#a1a1ff",
    "TH-20": "#a1a1ff",
    "TH-21": "#a1a1ff",
    "TH-23": "#a1a1ff",
    "TH-41": "#fefcd5",
    "TH-39": "#fefcd5",
    "TH-43": "#fefcd5",
    "TH-38": "#fefcd5",
    "TH-42": "#fefcd5",
    "TH-48": "#fafc8e",
    "TH-49": "#fafc8e",
    "TH-47": "#fafc8e",
    "TH-45": "#ffff16",
    "TH-40": "#ffff16",
    "TH-44": "#ffff16",
    "TH-46": "#ffff16",
    "TH-37": "#ffe365",
    "TH-34": "#ffe365",
    "TH-35": "#ffe365",
    "TH-33": "#ffe365",
    "TH-32": "#f7c00b",
    "TH-30": "#f7c00b",
    "TH-31": "#f7c00b",
    "TH-36": "#f7c00b",
    "TH-50": "#a05c11",
    "TH-58": "#a05c11",
    "TH-52": "#a05c11",
    "TH-51": "#a05c11",
    "TH-55": "#c87b23",
    "TH-56": "#c87b23",
    "TH-57": "#c87b23",
    "TH-54": "#c87b23",
    "TH-63": "#dc9d50",
    "TH-65": "#dc9d50",
    "TH-64": "#dc9d50",
    "TH-67": "#dc9d50",
    "TH-53": "#dc9d50",
    "TH-62": "#eabb87",
    "TH-66": "#eabb87",
    "TH-60": "#eabb87",
    "TH-61": "#eabb87"
  }

  code_color2 = {
    "TH-10": "#a05c11",
    "TH-12": "#c87b23",
    "TH-13": "#c87b23",
    "TH-14": "#c87b23",
    "TH-19": "#dc9d50",
    "TH-15": "#dc9d50",
    "TH-16": "#c87b23",
    "TH-17": "#dc9d50",
    "TH-18": "#dc9d50",
    "TH-24": "#dc9d50",
    "TH-25": "#c87b23",
    "TH-27": "#c87b23",
    "TH-26": "#dc9d50",
    "TH-11": "#dc9d50",
    "TH-71": "#c87b23",
    "TH-73": "#c87b23",
    "TH-70": "#dc9d50",
    "TH-72": "#dc9d50",
    "TH-77": "#c87b23",
    "TH-76": "#dc9d50",
    "TH-74": "#dc9d50",
    "TH-75": "#dc9d50",
    "TH-86": "#dc9d50",
    "TH-84": "#c87b23",
    "TH-80": "#dc9d50",
    "TH-93": "#dc9d50",
    "TH-85": "#dc9d50",
    "TH-82": "#c87b23",
    "TH-83": "#c87b23",
    "TH-81": "#dc9d50",
    "TH-92": "#c87b23",
    "TH-90": "#dc9d50",
    "TH-91": "#dc9d50",
    "TH-94": "#dc9d50",
    "TH-95": "#dc9d50",
    "TH-96": "#c87b23",
    "TH-22": "#dc9d50",
    "TH-20": "#c87b23",
    "TH-21": "#dc9d50",
    "TH-23": "#c87b23",
    "TH-41": "#dc9d50",
    "TH-39": "#dc9d50",
    "TH-43": "#dc9d50",
    "TH-38": "#dc9d50",
    "TH-42": "#a05c11",
    "TH-48": "#dc9d50",
    "TH-49": "#c87b23",
    "TH-47": "#dc9d50",
    "TH-45": "#c87b23",
    "TH-40": "#c87b23",
    "TH-44": "#dc9d50",
    "TH-46": "#c87b23",
    "TH-37": "#c87b23",
    "TH-34": "#dc9d50",
    "TH-35": "#c87b23",
    "TH-33": "#c87b23",
    "TH-32": "#a05c11",
    "TH-30": "#c87b23",
    "TH-31": "#dc9d50",
    "TH-36": "#a05c11",
    "TH-50": "#a05c11",
    "TH-58": "#c87b23",
    "TH-52": "#c87b23",
    "TH-51": "#c87b23",
    "TH-55": "#a05c11",
    "TH-56": "#c87b23",
    "TH-57": "#c87b23",
    "TH-54": "#c87b23",
    "TH-63": "#a05c11",
    "TH-65": "#a05c11",
    "TH-64": "#c87b23",
    "TH-67": "#c87b23",
    "TH-53": "#c87b23",
    "TH-62": "#c87b23",
    "TH-66": "#c87b23",
    "TH-60": "#c87b23",
    "TH-61": "#dc9d50"
  }


  codeMapping: any = {
    "TH-10": "กรุงเทพมหานคร",
    "TH-11": "สมุทรปราการ",
    "TH-12": "นนทบุรี",
    "TH-13": "ปทุมธานี",
    "TH-14": "พระนครศรีอยุธยา",
    "TH-15": "อ่างทอง",
    "TH-16": "ลพบุรี",
    "TH-17": "สิงห์บุรี",
    "TH-18": "ชัยนาท",
    "TH-19": "สระบุรี",
    "TH-20": "ชลบุรี",
    "TH-21": "ระยอง",
    "TH-22": "จันทบุรี",
    "TH-23": "ตราด",
    "TH-24": "ฉะเชิงเทรา",
    "TH-25": "ปราจีนบุรี",
    "TH-26": "นครนายก",
    "TH-27": "สระแก้ว",
    "TH-30": "นครราชสีมา",
    "TH-31": "บุรีรัมย์",
    "TH-32": "สุรินทร์",
    "TH-33": "ศรีสะเกษ",
    "TH-34": "อุบลราชธานี",
    "TH-35": "ยโสธร",
    "TH-36": "ชัยภูมิ",
    "TH-37": "อำนาจเจริญ",
    "TH-38": "บึงกาฬ",
    "TH-39": "หนองบัวลำภู",
    "TH-40": "ขอนแก่น",
    "TH-41": "อุดรธานี",
    "TH-42": "เลย",
    "TH-43": "หนองคาย",
    "TH-44": "มหาสารคาม",
    "TH-45": "ร้อยเอ็ด",
    "TH-46": "กาฬสินธุ์",
    "TH-47": "สกลนคร",
    "TH-48": "นครพนม",
    "TH-49": "มุกดาหาร",
    "TH-50": "เชียงใหม่",
    "TH-51": "ลำพูน",
    "TH-52": "ลำปาง",
    "TH-53": "อุตรดิตถ์",
    "TH-54": "แพร่",
    "TH-55": "น่าน",
    "TH-56": "พะเยา",
    "TH-57": "เชียงราย",
    "TH-58": "แม่ฮ่องสอน",
    "TH-60": "นครสวรรค์",
    "TH-61": "อุทัยธานี",
    "TH-62": "กำแพงเพชร",
    "TH-63": "ตาก",
    "TH-64": "สุโขทัย",
    "TH-65": "พิษณุโลก",
    "TH-66": "พิจิตร",
    "TH-67": "เพชรบูรณ์",
    "TH-70": "ราชบุรี",
    "TH-71": "กาญจนบุรี",
    "TH-72": "สุพรรณบุรี",
    "TH-73": "นครปฐม",
    "TH-74": "สมุทรสาคร",
    "TH-75": "สมุทรสงคราม",
    "TH-76": "เพชรบุรี",
    "TH-77": "ประจวบคีรีขันธ์",
    "TH-80": "นครศรีธรรมราช",
    "TH-81": "กระบี่",
    "TH-82": "พังงา",
    "TH-83": "ภูเก็ต",
    "TH-84": "สุราษฎร์ธานี",
    "TH-85": "ระนอง",
    "TH-86": "ชุมพร",
    "TH-90": "สงขลา",
    "TH-91": "สตูล",
    "TH-92": "ตรัง",
    "TH-93": "พัทลุง",
    "TH-94": "ปัตตานี",
    "TH-95": "ยะลา",
    "TH-96": "นราธิวาส"
  }
  // data
  public lineChartData: Array<number> = [1, 8, 49];

  public barChartData: ChartDataSets[]
  public barChartData2: ChartDataSets[]
  public barChartData3: ChartDataSets[]
  public barChartData6: ChartDataSets[]
  public barChartData4: ChartDataSets[]
  public barChartData5: ChartDataSets[]
  lineChartOptions3: any
  lineChartOptions4: any
  lineChartOptions5: any
  lineChartOptions6: any

  // public barChartData: ChartData<'bar'> = {
  //   labels: ['2006', '2007', '2008', '2009', '2010', '2011', '2012'],
  //   datasets: [
  //     { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
  //     { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
  //   ],
  // };

  public labelMFL: Array<any> = [
    {
      data: this.lineChartData,
      label: this.SystemName,
    }
  ];
  // labels
  public lineChartLabels: Array<any> = ["แกนนำอาสาสมัครดิจิทัล"];
  public lineChartLabels2: Array<any> = ["อาสาสมัครดิจิทัล"];
  public lineChartLabels3: Array<any> = ["ช่วงอายุ"];
  public lineChartLabels4: Array<any> = ['เพศหญิง', 'เพศชาย'];

  constructor(private homeService: HomeService,
    private renderer: Renderer2) {

  }

  ngOnInit(): void {
    $.getScript('../../../../assets/js/custom.js');
    $('.loader').fadeIn('slow');
    this.getDashboardProvinnce(this.province)
    this.getDashboard()
    this.getDashboardAmphoe(this.province)
   
    this.colorMap()
   

    // this.thaimap();
    // this.loadJVectorMapScripts();
  }

  public selectedChart: string = 'province'; // ค่าเริ่มต้นเป็น province
  public selectedChart1: string = 'province1'; // ค่าเริ่มต้นเป็น province
  public selectedChart2: string = 'leader'; // ค่าเริ่มต้นเป็น province

  public selectChart(chart: string): void {
    this.selectedChart = chart;
  }
  public selectChart1(chart: string): void {
    this.selectedChart1 = chart;
  }
  public selectChart2(chart: string): void {
    this.selectedChart2 = chart;
  }

  public lineChartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          max: this.countUser?.digitalVillage,
          min: 0,
        }
      }],
      xAxes: [{


      }],
    },
    plugins: {
      datalabels: {
        display: true,
        align: 'top',
        anchor: 'end',
        //color: "#2756B3",
        color: "#222",

        font: {
          family: 'Prompt',
          size: 14
        },

      },
      deferred: false

    },

  };

  _lineChartColors: Array<any> = [{
    backgroundColor: 'red',
    borderColor: 'red',
    pointBackgroundColor: 'red',
    pointBorderColor: 'red',
    pointHoverBackgroundColor: 'red',
    pointHoverBorderColor: 'red'
  }];



  public ChartType = 'bar';
  public ChartType1 = 'doughnut';
  lineChartOptions2: any

  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = ['Red', 'Blue', 'Yellow', 'Green', 'Purple'];
  public pieChartData: number[] = [300, 200, 100, 400, 500];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  // public donutChartData: number[] = [300, 500];


  public chartClicked(e: any): void {
    // console.log(e);
  }
  public chartHovered(e: any): void {
    // console.log(e);
  }

  getDashboard() {
    // console.log("year",this.year);
    this.homeService.reportDashboard(this.year).subscribe(result => {
     
      // console.log("getDashboard",result[0]);
      
      // alert(JSON.stringify(result))
      this.countUser = result[0]
      // console.log(this.countUser.digitalVillage)
      this.barChartData =
        [
          {
            data: [this.countUser.Leader],
            label: 'แกนนำอาสาสมัครดิจิทัล',
            backgroundColor: "#FFCDD2",
            borderColor: "#FFCDD2",
            hoverBackgroundColor: "#FFCDD2AA",
            hoverBorderColor: "#FFCDD2AA",
          },
          {
            data: [this.countUser.Leaderpass],
            label: 'ผ่านแบบประเมิน',
            backgroundColor: "#EF5350",
            borderColor: "#EF5350",
            hoverBackgroundColor: "#EF5350AA",
            hoverBorderColor: "#EF5350AA",

          }
        ];
      
      this.barChartData2 = [
        {
          data: [this.countUser.useronde],
          label: 'อาสาสมัครดิจิทัล',
          // backgroundColor: "rgba(43, 17, 14, 0.9)",
          // borderColor: "rgba(43, 17, 14, 0.9)",
          backgroundColor: "#D1C4E9",
          borderColor: "#D1C4E9",
          hoverBackgroundColor: "#D1C4E9AA",
          hoverBorderColor: "#D1C4E9AA",
        },
        {
          data: [this.year == 2024 ? this.countUser.digitalpass : this.countUser?.digitalvillagepass + this.countUser?.digitalpasspass],
          label: 'ผ่านแบบประเมิน',
          backgroundColor: "#7E57C2",
          borderColor: "#7E57C2",
          hoverBackgroundColor: "#7E57C2AA",
          hoverBorderColor: "#7E57C2AA",

        }
      ];

      this.lineChartOptions = {
        responsive: true,
        hover: {
          mode: 'nearest', // hover จะทำงานที่จุดใกล้ที่สุด
          intersect: true,  // ให้แสดง hover เมื่อ cursor ตรงกับจุดในกราฟ
        },
        scales: {
          yAxes: [{
            ticks: {
              stepSize: 50000,
              max: this.countUser.Leader,
              min: 0,
              fontFamily: 'Prompt' // Add font family here
            }
          }],
          xAxes: [{
            ticks: {
              fontFamily: 'Prompt' // Add font family here
            },
            barPercentage: 0.7,  // ปรับลดขนาดของแท่งในแนวนอน
            categoryPercentage: 0.7,  // ปรับความกว้างของกลุ่มแท่ง
          }],
        },
        plugins: {
          datalabels: {
            display: true,
            align: 'bottom',
            anchor: 'end',
            color: '#fff',
            backgroundColor: '#404040',

            font: {
              family: 'Prompt',
              size: 14
            },
            formatter: function(value, context) {
              // Format the number with commas
              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
          },
          deferred: false

        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              // Get the value from the dataset
              const value = tooltipItem.yLabel || tooltipItem.value;
              // Format the number with commas
              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            },
            labelTextColor: function (tooltipItem, chart) {
              return '#fff';
            },
          },
          bodyFontFamily: 'Prompt', // Add font family here
          titleFontFamily: 'Prompt', // Add font family here
        },
        legend: {
          labels: {
            fontFamily: 'Prompt' // Add font family here
          }
        },
     

      };
      this.lineChartOptions2 = {
        responsive: true,
        hover: {
          mode: 'nearest', // hover จะทำงานที่จุดใกล้ที่สุด
          intersect: true,  // ให้แสดง hover เมื่อ cursor ตรงกับจุดในกราฟ
        },
        scales: {
          yAxes: [{
            ticks: {
              stepSize: 50000,
              max: this.countUser?.useronde > this.countUser?.digitalpass ? this.countUser?.useronde : this.countUser?.digitalpass,
              min: 0,
              fontFamily: 'Prompt' // Add font family here
            }
          }],
          xAxes: [{
            ticks: {
              fontFamily: 'Prompt' // Add font family here
            },
            barPercentage: 0.7,  // ปรับลดขนาดของแท่งในแนวนอน
            categoryPercentage: 0.7,  // ปรับความกว้างของกลุ่มแท่ง
          }],
        },
        plugins: {
          datalabels: {
            display: true,
            align: 'bottom',
            anchor: 'end',
            color: '#fff',
            backgroundColor: '#404040',

            font: {
              family: 'Prompt',
              size: 14
            },
            formatter: function(value, context) {
              // Format the number with commas
              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

          },
          deferred: false
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              // Get the value from the dataset
              const value = tooltipItem.yLabel || tooltipItem.value;
              // Format the number with commas
              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            },
            labelTextColor: function (tooltipItem, chart) {
              return '#fff';
            },
          },
          bodyFontFamily: 'Prompt', // Add font family here
          titleFontFamily: 'Prompt', // Add font family here
        },
        legend: {
          labels: {
            fontFamily: 'Prompt' // Add font family here
          }
        }

      };

      this.loading = true
    })
  }

  onChange(deviceValue) {
    //  this.year = deviceValue 
    $('.loader').fadeIn('slow');
    this.getDashboard()
    this.getDashboardProvinnce(this.province)
    this.getDashboardAmphoe(this.province)

  
  }
  male:any = ""
  female:any = ""
  min:any = 0
  max:any = 0
  testscorepass:any = 0
  groupage:any = ""
  testlevel:any = ""
  // barChartData4
  getDashboardProvinnce(province) {
    this.homeService.reportDashboardProvince(province, this.year).subscribe(result => {
      // console.log('reportDashboardProvince', result);

      this.countuserProvince = result[0].asdvolunteer
      this.male = result[0].male
      this.female = result[0].female
      this.testlevel = result[0].testlevel

      let arr: any = Object.values(result[0].groupage);
      let min = Math.min(...arr);
      let max = Math.max(...arr);

      let arr1: any = Object.values(result[0].allgroupage);
      let max1 = Math.max(...arr1);

      // console.log(`Min value: ${min}, max value: ${max}`);
      // this.groupage = result.groupage
      this.barChartData3 = [
        {
          data: [result[0].groupage?.genz],
          label: 'กลุ่ม  Generation Z (ช่วงอายุ 6-24 ปี)',
          backgroundColor: "#FFCDD2",
          borderColor: "#FFCDD2",
          hoverBackgroundColor: "#FFCDD2aa",
          hoverBorderColor: "#FFCDD2aa",
        },
        {
          data: [result[0].groupage?.geny],
          label: 'กลุ่ม  Generation Y (ช่วงอายุ 25-42 ปี)',
          backgroundColor: "#EF9A9A",
          borderColor: "#EF9A9A",
          hoverBackgroundColor: "#EF9A9Aaa",
          hoverBorderColor: "#EF9A9Aaa",

        },
        {
          data: [result[0].groupage?.genx],
          label: 'กลุ่ม  Generation X (ช่วงอายุ 43-57 ปี)',
          backgroundColor: "#E57373",
          borderColor: "#E57373",
          hoverBackgroundColor: "#E57373aa",
          hoverBorderColor: "#E57373aa",
        },
        {
          data: [result[0].groupage?.baby],
          label: 'กลุ่ม Baby Boomer (ช่วงอายุ 58-76 ปี)',
          backgroundColor: "#EF5350",
          borderColor: "#EF5350",
          hoverBackgroundColor: "#EF5350aa",
          hoverBorderColor: "#EF5350aa",
        },
      ];

      this.barChartData4 = [
        {
          data: [result[0].female, result[0].male],
          label: 'Gender Distribution',
          backgroundColor: ["rgba(255, 99, 132, 0.7)", "rgba(54, 162, 235, 0.7)",],
          borderColor: ["rgba(255, 99, 132, 0.7)", "rgba(54, 162, 235, 0.7)",],
          hoverBackgroundColor: ["rgba(255, 99, 132, 0.5)", "rgba(54, 162, 235, 0.5)"],
          hoverBorderColor: ["rgba(255, 99, 132, 0.5)", "rgba(54, 162, 235, 0.5)"],
        },
      ];
      this.barChartData5 = [
        {
          data: [result[0].allfemale, result[0].allmale],
          label: 'Gender Distribution',
          backgroundColor: ["rgba(255, 99, 132, 0.7)", "rgba(54, 162, 235, 0.7)",],
          borderColor: ["rgba(255, 99, 132, 0.7)", "rgba(54, 162, 235, 0.7)",],
          hoverBackgroundColor: ["rgba(255, 99, 132, 0.5)", "rgba(54, 162, 235, 0.5)"],
          hoverBorderColor: ["rgba(255, 99, 132, 0.5)", "rgba(54, 162, 235, 0.5)"],
        },
      ];
      this.lineChartOptions3 = {
        responsive: true,
        hover: {
          mode: 'nearest', // hover จะทำงานที่จุดใกล้ที่สุด
          intersect: true,  // ให้แสดง hover เมื่อ cursor ตรงกับจุดในกราฟ
        },
        scales: {
          yAxes: [{
            ticks: {
              stepSize: 50,
              max: max,
              min: 0,
              fontFamily: 'Prompt' // Add font family here
            }
          }],
          xAxes: [{
            ticks: {
              fontFamily: 'Prompt' // Add font family here
            }
          }],
        },
        plugins: {
          datalabels: {
            display: true,
            align: 'bottom',
            anchor: 'end',
            color: '#fff',
            backgroundColor: '#404040',

            font: {
              family: 'Prompt',
              size: 14
            },
            formatter: function(value, context) {
              // Format the number with commas
              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

          },
          deferred: false
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              // Get the value from the dataset
              const value = tooltipItem.yLabel || tooltipItem.value;
              // Format the number with commas
              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            },
            labelTextColor: function (tooltipItem, chart) {
              return '#fff';
            },
          },
          bodyFontFamily: 'Prompt', // Add font family here
          titleFontFamily: 'Prompt', // Add font family here
        },
        legend: {
          labels: {
            fontFamily: 'Prompt' // Add font family here
          }
        }

      };

      this.barChartData6 = [
        {
          data: [result[0].allgroupage?.genz],
          label: 'กลุ่ม  Generation Z (ช่วงอายุ 6-24 ปี)',
          backgroundColor: "#D1C4E9",
          borderColor: "#D1C4E9",
          hoverBackgroundColor: "#D1C4E9AA",
          hoverBorderColor: "#D1C4E9AA",
        },
        {
          data: [result[0].allgroupage?.geny],
          label: 'กลุ่ม  Generation Y (ช่วงอายุ 25-42 ปี)',
          backgroundColor: "#B39DDB",
          borderColor: "#B39DDB",
          hoverBackgroundColor: "#B39DDBAA",
          hoverBorderColor: "#B39DDBAA",

        },
        {
          data: [result[0].allgroupage?.genx],
          label: 'กลุ่ม  Generation X (ช่วงอายุ 43-57 ปี)',
          backgroundColor: "#9575CD",
          borderColor: "#9575CD",
          hoverBackgroundColor: "#9575CDAA",
          hoverBorderColor: "#9575CDAA",
        },
        {
          data: [result[0].allgroupage?.baby],
          label: 'กลุ่ม Baby Boomer (ช่วงอายุ 58-76 ปี)',
          backgroundColor: "#7E57C2",
          borderColor: "#7E57C2",
          hoverBackgroundColor: "#7E57C2AA",
          hoverBorderColor: "#7E57C2AA",
        },
      ];

      this.lineChartOptions4 = {
        responsive: true,
        plugins: {
          datalabels: {
            display: true,
            align: 'top',
            anchor: 'end',
            color: '#fff',
            backgroundColor: '#404040',
            font: {
              family: 'Prompt',
              size: 14,
            },
            formatter: (value, ctx) => {
              const dataArr = ctx.chart.data.datasets[0].data;
              const sum = dataArr.reduce((acc, data) => acc + data, 0); // Calculate the sum of all data points
              const percentage = ((value * 100) / sum).toFixed(2) + '%'; // Calculate percentage
              return percentage;
            },
          },
          deferred: false,
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              // Get the dataset and value for the hovered item
              const dataset = data.datasets[tooltipItem.datasetIndex];
              const value = dataset.data[tooltipItem.index];
              // Format the value with commas
              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            },
            labelTextColor: function () {
              return '#fff'; // Tooltip text color
            },
          },
          bodyFontFamily: 'Prompt', // Font family for tooltip body
          titleFontFamily: 'Prompt', // Font family for tooltip title
        },
        legend: {
          labels: {
            fontFamily: 'Prompt', // Font family for legend labels
          },
        },
      };
      

      this.lineChartOptions5 = {
        responsive: true,
        plugins: {
          datalabels: {
            display: true,
            align: 'top',
            anchor: 'end',
            color: '#fff',
            backgroundColor: '#404040',
            font: {
              family: 'Prompt',
              size: 14,
            },
            formatter: (value, ctx) => {
              const dataArr = ctx.chart.data.datasets[0].data;
              const sum = dataArr.reduce((acc, data) => acc + data, 0); // Calculate the sum of all data points
              const percentage = ((value * 100) / sum).toFixed(2) + '%'; // Calculate percentage
              return percentage;
            },
          },
          deferred: false,
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              // Get the dataset and value for the hovered item
              const dataset = data.datasets[tooltipItem.datasetIndex];
              const value = dataset.data[tooltipItem.index];
              // Format the value with commas
              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            },
            labelTextColor: function () {
              return '#fff'; // Tooltip text color
            },
          },
          bodyFontFamily: 'Prompt', // Font family for tooltip body
          titleFontFamily: 'Prompt', // Font family for tooltip title
        },
        legend: {
          labels: {
            fontFamily: 'Prompt', // Font family for legend labels
          },
        },
      };
      
      

      this.lineChartOptions6 = {
        responsive: true,
        scales: {
          yAxes: [{
            ticks: {
              stepSize: 3000,
              max: max1,
              min: 0,
              fontFamily: 'Prompt' // Add font family here
            }
          }],
          xAxes: [{
            ticks: {
              fontFamily: 'Prompt' // Add font family here
            }
          }],
        },
        plugins: {
          datalabels: {
            display: true,
            align: 'bottom',
            anchor: 'end',
            color: '#fff',
            backgroundColor: '#404040',

            font: {
              family: 'Prompt',
              size: 14
            },
            formatter: function(value, context) {
              // Format the number with commas
              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

          },
          deferred: false
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              // Get the value from the dataset
              const value = tooltipItem.yLabel || tooltipItem.value;
              // Format the number with commas
              return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            },
            labelTextColor: function (tooltipItem, chart) {
              return '#fff';
            },
          },
          bodyFontFamily: 'Prompt', // Add font family here
          titleFontFamily: 'Prompt', // Add font family here
        },
        legend: {
          labels: {
            fontFamily: 'Prompt' // Add font family here
          }
        }

      };

      // this.min = result[1]
      // this.max = result[3]
      // $('.loader').fadeOut('slow');
      this.testscorepass = result[0].testscorepass
      $('.loader').fadeOut('slow');
    })
  }
  amphoes
  loadingamphoe = false
  getDashboardAmphoe(province) {
    this.homeService.reportDashboardAmphoe(province,this.year).subscribe(result => {
      // console.log(result)
      this.amphoes = result
      this.loadingamphoe = true
      // $('.loader').fadeOut('slow');
    })
  }


  loadJVectorMapScripts(): void {
    scriptjs('../../../../assets/jvectormap/jquery-jvectormap-2.0.3.min.js', 'jvectormap', () => {
      scriptjs('../../../../assets/jvectormap/jquery-jvectormap-th-mill.js', 'jvectormap-th', () => {
        // ทำงานเมื่อไฟล์ jvectormap โหลดเสร็จ
        alert('112')

      });
    });
  }


  thaimap() {

    // const self = this;
    $(this.thaiMap.nativeElement).vectorMap({

      map: "th_mill",
      zoomButtons: false,
      enableZoom: true,
      zoomOnScroll: false,
      backgroundColor: "none",
      markerStyle: {
        initial: {
          fill: "#ff0000",
          stroke: "#000000"
        }
      },
      regionStyle: {
        initial: {
          fill: "#999999",
          "fill-opacity": 1,
          stroke: "#000000",
          "stroke-width": 1,
          "stroke-opacity": 1
        },
        hover: {
          cursor: "pointer"
        }
      },
      series: {
        regions: [
          {
            normalizeFunction: "polynomial",
            attribute: "fill",
            values: this.code_color2
          }
        ]
      },
      //   onRegionTipShow: function(event, label, code) {
      //     console.log(122)
      //     // ตัวอย่างข้อมูลจำลองชื่อที่ต้องการแสดงเมื่อ hover
      //     const customNames = {
      //         "TH-10": "<center>กรุงเทพมหานคร <br> จำนวนอาสาสมัครดิจิทัล  : 5000</div></center>" ,
      //         "TH-34": "ชื่อที่ต้องการแสดงสำหรับพื้นที่ TH-30",
      //         // เพิ่มข้อมูลชื่อที่ต้องการแสดงสำหรับพื้นที่อื่น ๆ ต่อไปตามต้องการ
      //     };

      //     // หาชื่อที่ต้องการแสดงจาก customNames ถ้ามี ไม่เช่นนั้นใช้รหัสพื้นที่เดิม
      //     const customName = customNames[code] ? custom  Names[code] : code;

      //     // กำหนดข้อความที่ต้องการแสดงใน Label
      //     label.html(customName);
      // },


      onRegionClick: (event, code, region) => {


        // ใช้รหัสใหม่หรือรหัสเดิมถ้าไม่มีใน mapping
        // console.log(region);
        this.mapreport(code);
      }

    });

    //this.map.series.regions[0].setValues({ "TH-20": '#ff0000' });
  }

  loadScript(src: string): Promise<void> {
    return new Promise((resolve, reject) => {
      const scriptElement = this.renderer.createElement('script');
      this.renderer.setAttribute(scriptElement, 'src', src);
      this.renderer.listen(scriptElement, 'load', () => resolve());
      this.renderer.listen(scriptElement, 'error', (error) => reject(error));
      this.renderer.appendChild(document.body, scriptElement);
    });
  }

  mapreport(code) {
    // $('.loader').fadeIn('slow');
    const province = this.codeMapping[code];
    this.province = province
    this.getDashboardProvinnce(province)
    this.getDashboardAmphoe(province)
    
  }

//  ProvinceCounnt
  colorMap = async() =>{
      this.homeService.colorMap(this.year).subscribe(result => {
        // console.log(result)
        const mapData = this.arrayToObject(result);
        // console.log(mapData)
       
          this.code_color2 = mapData
          // console.log(this.code_color2)
          this.loadScript('../../../../assets/jvectormap/jquery-jvectormap-2.0.3.min.js').then(() => {
            this.loadScript('../../../../assets/jvectormap/jquery-jvectormap-th-mill.js').then(() => {
              // console.log(this.code_color2)
              this.thaimap()
            });
          });
          // console.log('23',this.ProvinceCounnt)/
      })
      $('.loader').fadeOut('slow');
  }

  arrayToObject(array) {
    return array.reduce((acc, curr) => {
        const key = Object.keys(curr)[0];
        acc[key] = curr[key];
        return acc;
    }, {});
}



}
