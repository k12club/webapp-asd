import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetlinksComponent } from './assetlinks.component';

describe('AssetlinksComponent', () => {
  let component: AssetlinksComponent;
  let fixture: ComponentFixture<AssetlinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetlinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetlinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
