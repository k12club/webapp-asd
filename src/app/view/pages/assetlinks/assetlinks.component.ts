import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/services/home.service';

@Component({
  selector: 'app-assetlinks',
  templateUrl: './assetlinks.component.html',
  styleUrls: ['./assetlinks.component.scss']
})
export class AssetlinksComponent implements OnInit {
  assetLinks
  constructor(
    private homeService : HomeService
  )
   {
 
   }

  ngOnInit(): void {
    this.homeService.getAssetLinks().subscribe((data: any) => {
      this.assetLinks = data
      // console.log(this.assetLinks)
    });
  }

}
