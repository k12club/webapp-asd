import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PreloaderComponent } from './components/common/preloader/preloader.component';
import { NavbarStyleOneComponent } from './components/common/navbar-style-one/navbar-style-one.component';
import { HomeOneComponent } from './components/pages/home-one/home-one.component';
import { HomeTwoComponent } from './components/pages/home-two/home-two.component';
import { HomeThreeComponent } from './components/pages/home-three/home-three.component';
import { FooterStyleOneComponent } from './components/common/footer-style-one/footer-style-one.component';
import { FooterStyleTwoComponent } from './components/common/footer-style-two/footer-style-two.component';
import { NavbarStyleTwoComponent } from './components/common/navbar-style-two/navbar-style-two.component';
import { AboutComponent } from './components/pages/about/about.component';
import { CategoriesComponent } from './components/pages/categories/categories.component';
import { ServicesComponent } from './components/pages/services/services.component';
import { ServicesDetailsComponent } from './components/pages/services-details/services-details.component';
import { BlogDetailsComponent } from './components/pages/blog-details/blog-details.component';
import { BlogComponent } from './components/pages/blog/blog.component';
import { FoodCollectionComponent } from './components/pages/food-collection/food-collection.component';
import { OnlineOrderComponent } from './components/pages/online-order/online-order.component';
import { ChefsComponent } from './components/pages/chefs/chefs.component';
import { BookTableComponent } from './components/pages/book-table/book-table.component';
import { CartComponent } from './components/pages/cart/cart.component';
import { CheckoutComponent } from './components/pages/checkout/checkout.component';
import { ComingSoonComponent } from './components/pages/coming-soon/coming-soon.component';
import { FaqComponent } from './components/pages/faq/faq.component';
import { TermsConditionsComponent } from './components/pages/terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './components/pages/privacy-policy/privacy-policy.component';
import { ErrorComponent } from './components/pages/error/error.component';
import { ContactComponent } from './components/pages/contact/contact.component';
import { NavbarStyleThreeComponent } from './components/common/navbar-style-three/navbar-style-three.component';
import { HttpClientModule } from '@angular/common/http'
import { ThaiDatePipe } from './components/pages/services/pipe/thaidate.service';
import { NewsGeneralComponent } from './components/pages/news-general/news-general.component'
import { KnowledgeComponent } from './components/pages/knowlegde/knowledge.component'

import { DataTablesModule } from 'angular-datatables';
import { HealtyComponent } from './components/pages/healty/healty.component';
import { LoginComponent } from './services/auth/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginNavComponent } from './services/component/login-nav/login-nav.component';
import { NewGeneralDetailsComponent } from './components/pages/new-general-details/new-general-details.component';
import { FacebookModule } from 'ngx-facebook';
import { ElearningComponent } from './components/pages/elearning/elearning.component';
import { ElearingDetailsComponent } from './components/pages/elearning/elearing-details/elearing-details.component';
import {RegulationTrainingComponent} from './components/pages/regulation-training/regulation-training.component'
import { ChecklistTrainingComponent } from './components/pages/checklist-training/checklist-training.component';
import { VillageComponent } from './components/pages/faq/village/village.component';
import { QuestionComponent } from './components/pages/question/question.component';
import { CommonModule } from '@angular/common';
// import { asset }
import { AssetlinksComponent } from './view/pages/assetlinks/assetlinks.component';
import { DashboardComponent } from './views/pages/dashboard/dashboard.component';
import { ChartsModule } from 'ng2-charts';
import { AuthComponent } from './components/pages/auth/auth.component';
import { AuthmobileComponent } from './views/pages/authmobile/authmobile.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DigitalComponent } from './views/pages/digital/digital.component';
import { AfncComponent } from './views/pages/afnc/afnc.component';
import { AuthmobileOndeComponent } from './views/pages/authmobileonde/authmobileonde.component';
@NgModule({
  declarations: [
    AppComponent,
    PreloaderComponent,
    NavbarStyleOneComponent,
    HomeOneComponent,
    HomeTwoComponent,
    HomeThreeComponent,
    FooterStyleOneComponent,
    FooterStyleTwoComponent,
    NavbarStyleTwoComponent,
    AboutComponent,
    CategoriesComponent,
    ServicesComponent,
    ServicesDetailsComponent,
    BlogDetailsComponent,
    BlogComponent,
    FoodCollectionComponent,
    OnlineOrderComponent,
    ChefsComponent,
    BookTableComponent,
    CartComponent,
    CheckoutComponent,
    ComingSoonComponent,
    FaqComponent,
    TermsConditionsComponent,
    PrivacyPolicyComponent,
    ErrorComponent,
    ContactComponent,
    NavbarStyleThreeComponent,
    ThaiDatePipe,
    NewsGeneralComponent,
    KnowledgeComponent,
    HealtyComponent,
    LoginComponent,
    LoginNavComponent,
    NewGeneralDetailsComponent,
    ElearningComponent,
    ElearingDetailsComponent,
    RegulationTrainingComponent,
    ChecklistTrainingComponent,
    VillageComponent,
    QuestionComponent , 
    AssetlinksComponent , 
    DashboardComponent , 
    AuthComponent ,
    AuthmobileComponent ,
    AuthmobileOndeComponent,
    DigitalComponent ,
    AfncComponent
  ],
  imports: [
    DataTablesModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    FacebookModule.forRoot(),
    ReactiveFormsModule,
    CommonModule ,
    ChartsModule ,
    FontAwesomeModule,
    // FontAwesomeModule
  ],
  exports: [
    ThaiDatePipe,
  
  ],
  providers: [],
  bootstrap: [AppComponent ]
})
export class AppModule { }
