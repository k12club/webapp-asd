// Generated by https://quicktype.io

export interface NewsAsd {
    id: number;
    name: string;
    description: string;
    headerimage: string;
    like: number;
    view: number;
    share: number;
    status: number;
    published: null;
    user_id: null;
    admin_id: number;
    created_at: string;
    updated_at: string;
    owner: Owner;
    images: Image[];
}

export interface Image {
    id: number;
    name: string;
    digital_news_id: number;
    created_at: string;
    updated_at: string;
}

export interface Owner {
    id: number;
    guiID: null;
    firstname: string;
    lastname: string;
    position: null;
    tel: string;
    email: string;
    role_id: number;
    created_at: string;
    updated_at: string;
}
export interface PR {
    id: number;
    Control: number;
    description: string;
    like: null;
    link: [];
    name: string;
    picture: string;
    share: null;
    status: number;
    created_at: string;
    updated_at: string;
    view:null;
}
