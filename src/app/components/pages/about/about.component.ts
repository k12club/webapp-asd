import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
  }
  openResgis() {    
    window.open(`http://tdv.netpracharat.com/admin/registerdigital`, "_blank");
  }
  
}
