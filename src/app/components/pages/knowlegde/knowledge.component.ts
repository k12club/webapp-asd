import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/services/home.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-knowlegde',
  templateUrl: './knowledge.component.html',
  styleUrls: ['./knowledge.component.scss']
})
export class KnowledgeComponent implements OnInit {

  knowledge: any;
  knowledgeDataPDF: any;
  knowledgeDataImage: any;
  knowledgeDataVideo: any;

  datamanual: any =[]

  constructor(
    private _homeService: HomeService,
    private router: Router
  ) { }

  ngOnInit(): void {
    // this.getKnowledge()

    this.getManual()
  }

  getManual(){
    this._homeService.getManual().subscribe( (result:any) => {
      // console.log("Manual",result);
          this.datamanual = result.filter((data) => data.type == "ebook")
         
          // console.log(this.datamanual,'datamanual');
        
        }) 
  }

  getKnowledge() {
    // alert('in')
    this._homeService.getKnowledge()
      .subscribe(result => {

        this.knowledge = result.data;

        this.knowledgeDataPDF = this.knowledge.data;

        let newData = [];
        for (var i = 0; i < result.data.length; i++) {
          if (result.data[i].mediatype == 'เอกสารเพื่อการเรียนรู้ (pdf)') {
            newData.push({
              id: result.data[i].id,
              medianame: result.data[i].medianame,
              mediadetail: result.data[i].mediadetail,
              headlines: result.data[i].headlines,
              mediatype: result.data[i].mediatype,
              created_at: result.data[i].created_at,
              uploadfile: result.data[i].uploadfile,


            })
          }
        }

        this.knowledgeDataPDF = newData

        this.knowledgeDataImage = this.knowledge.data;
        let newData1 = [];
        for (var i = 0; i < result.data.length; i++) {
          if (result.data[i].mediatype == 'สื่อรูปภาพเพื่อการเรียนรู้ (jpg , png)') {
            newData1.push({
              id: result.data[i].id,
              medianame: result.data[i].medianame,
              mediadetail: result.data[i].mediadetail,
              headlines: result.data[i].headlines,
              mediatype: result.data[i].mediatype,
              created_at: result.data[i].created_at,
              uploadfile: result.data[i].uploadfile,
            })
          }
        }

        this.knowledgeDataImage = newData1

        this.knowledgeDataVideo = this.knowledge.data;
        let newData2 = [];
        for (var i = 0; i < result.data.length; i++) {
          if (result.data[i].mediatype == 'สื่อวิดีโอเพื่อการเรียนรู้ (mp4)') {
            newData2.push({
              id: result.data[i].id,
              medianame: result.data[i].medianame,
              mediadetail: result.data[i].mediadetail,
              headlines: result.data[i].headlines,
              mediatype: result.data[i].mediatype,
              created_at: result.data[i].created_at,
              uploadfile: result.data[i].uploadfile,
            })
          }
        }

        this.knowledgeDataVideo = newData2



        // console.log(this.knowledgeDataPDF,'pdf');
        // console.log(this.knowledgeDataImage,'Image');
        // console.log(this.knowledgeDataVideo,'Video');
        // console.log(result);
      }
      )

  }

  openPDF(uploadfile) {
    // alert(uploadfile)


    // console.log(this.knowledgeDataVideo[1].uploadfile,'eoeoe');

    window.open(`http://npcradm.netpracharat.com/mediadetail/${uploadfile}`, "_blank");
  }

  openImage(uploadfile1) {
    // alert(uploadfile)


    // console.log(this.knowledgeDataVideo[1].uploadfile,'eoeoe');

    window.open(`http://npcradm.netpracharat.com/mediadetail/${uploadfile1}`, "_blank");
  }

  openVideo(uploadfile2) {
    // alert(uploadfile)


    // console.log(this.knowledgeDataVideo[1].uploadfile,'eoeoe');

    window.open(`http://npcradm.netpracharat.com/mediadetail/${uploadfile2}`, "_blank");
  }



  scrollTo(elementName) {
    let el = document.getElementById(elementName);
    window.scrollTo({ left: 0, top: el.offsetTop - 40, behavior: 'smooth' });
  }

   ////////////////////////////////// manual //////////////////////////////////
  openManual_1(file) {    
    // window.open(`https://drive.google.com/file/d/1lAVA1344Hc34rxfwsM9_fOalDM0PiGIT/view?usp=sharing`, "_blank");
    window.open(`${file}`, "_blank");  }
  
  openManual_2() {    
    window.open(`https://drive.google.com/file/d/1tdR2ChsOtr_SF2qupfU06BRF8giJymXZ/view?usp=sharing`, "_blank");
  }

  openManual_3() {    
    window.open(`https://drive.google.com/file/d/1v19L242YqgGhwsajP6eOEodDnr3bY0VD/view?usp=sharing`, "_blank");
  }

  openManual_4() {    
    window.open(`http://ebook04.sure.guru/`, "_blank");
  }

 

  ////////////////////////////////// course //////////////////////////////////
  openCourse_1() {    
    window.open(`https://drive.google.com/file/d/1S1_H7OikEzqlwFJNMB-DzQEBoxd-u2A3/view?usp=sharing`, "_blank");
  }
  

  openCourse_2() {    
    window.open(`https://drive.google.com/file/d/1OA1eEO3LhcZdeAY2RuWbEtVRzYeeDWs2/view?usp=sharing`, "_blank");
  }
  openCourse_3() {    
    window.open(`https://drive.google.com/file/d/1XP3vCo0w-Xb85ETakkeqhGcjU2mZdR7y/view?usp=sharing`, "_blank");
  }
  openCourse_4() {    
    window.open(`https://drive.google.com/file/d/1Rnfp6RfpgTlzE93LU5xrcJmUBzR8ndPm/view?usp=sharing`, "_blank");
  }
  
  openCourse_5() {    
    window.open(`https://drive.google.com/file/d/1hLB3RLFjKm7bmuxe87Ty4JoA0redQREU/view?usp=sharing`, "_blank");
  }
  
  openCourse_6() {    
    window.open(`https://drive.google.com/file/d/1oSHPnWe9r-_GrTGXL8L33H8XvT6Agzgl/view?usp=sharing`, "_blank");
  }
  
  openCourse_7() {    
    window.open(`https://drive.google.com/file/d/16lz2YCgTmMzv7dfr-XzTGrM7o1CmMzdR/view?usp=sharing`, "_blank");
  }
  

  ////////////////////////////////// presentation //////////////////////////////////
  
  


  

  ////////////////////////////////// Elearning //////////////////////////////////



////////////////////////////////// type2 //////////////////////////////////
  



////////////////////////////////// type3 //////////////////////////////////


////////////////////////////////// Drive Info and Video //////////////////////////////////

openInfoType1(){
  window.open(`https://drive.google.com/drive/folders/1Qz_zfl_ILGVjK3wbVdM4ddXR7Ghzt76T?usp=sharing`, "_blank");
  
}
openInfoType2(){
  window.open(`https://drive.google.com/drive/folders/1PR2-xrn9sGzHEou1nwh3yVuz39EE-uWI?usp=sharing`, "_blank");
  
}
openInfoType3(){
  window.open(`https://drive.google.com/drive/folders/11S3rRyFerEGSAlcVgJHysQPs1lwko3rO?usp=sharing`, "_blank");
  
}
openInfoType4(){
  window.open(`https://drive.google.com/drive/folders/1fXZC9hA21xqHPgkqP0NljzQhJNNDUhJo?usp=sharing`, "_blank");
  
}
openInfoType5(){
  window.open(`https://drive.google.com/drive/u/0/folders/1UbZ4Ca6V-e3tH_6N6-PZlWQG_FSLseO0`, "_blank");
  
}


}
