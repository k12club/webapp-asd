import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { NewsAsd } from 'src/app/models/newsAsd';
import { UserAsd } from 'src/app/services/auth/_models/asduser';
import { AuthenticationService } from 'src/app/services/auth/_services/authentication.service';
import { HomeService } from 'src/app/services/home.service';
import axios from 'axios';

@Component({
  selector: 'app-home-three',
  templateUrl: './home-three.component.html',
  styleUrls: ['./home-three.component.scss']
})
export class HomeThreeComponent implements OnInit {
  newsAsd: NewsAsd[] = []
  actAsd: any;
  newsAsa: any;

  knowledge: any;
  knowledgeDataPDF: any;
  knowledgeDataImage: any;
  knowledgeDataVideo: any;
  userAsd: UserAsd

  dataPR: any;
  description: any;
  description1: any;
  description2: any;
  description3: any;
  description4: any;
  description5: any;
  description6: any;
  description7: any;
  digitalUpdateHome: any = [];
  digitalUpdateHomeBanner: any = [];
  loading = true
  datamanual: any = []
  dataDCC: any = []
  dataGCC: any = []

  constructor
    (
      private _homeService: HomeService,
      private authenticationService: AuthenticationService,
      private router: Router) {
    this.userAsd = authenticationService.currentUserValue
  }

  @ViewChild('autoPopupModal') autoPopupModal!: ElementRef;

  ngAfterViewInit() {
    // Trigger the modal when the component view has been initialized
    // const currentDomain = window.location.hostname;
    // const newDomain = 'tdv.onde.go.th';
    // if (currentDomain !== newDomain) {
    //   this.autoPopupModal.nativeElement.classList.add('show');
    //   this.autoPopupModal.nativeElement.style.display = 'block';
    //   document.body.classList.add('modal-open', 'blur-background');
    // }
  }
  toggleModalClasses(isOpen: boolean) {
    const bodyClasses = document.body.classList;
    if (isOpen) {
      bodyClasses.add('modal-open', 'blur-background');
    } else {
      bodyClasses.remove('modal-open', 'blur-background');
    }
  }
  ngOnInit() {
    this.getActAsd()
    this.getAsaNews()
    this.getDigitalUpdate()
    this.getManual()
    // this.getCourseDCC()
    this.getGCC()
  }

  gotoNewWebsite() {
    window.open(`https://tdv.onde.go.th/home`, "_self");

  }
  getCourseDCC() {
    this._homeService.getDCC().subscribe((result: any) => {
      // console.log("Manual",result);
      this.dataDCC = result.data
      // console.log('dataDCC',this.dataDCC);
    })
  }

  gotoCouseOtherDetails(url) {
    window.open(url, "_blank");

  }
  getGCC() {
    this._homeService.getGCC().subscribe((result: any) => {
      // console.log("Manual",result);
      this.dataGCC = result
      // console.log('dataGCC',this.dataGCC);
    })
  }

  getManual() {
    this._homeService.getManual().subscribe((result: any) => {
      // console.log("Manual",result);
      this.datamanual = result.filter((data) => data.type == "ebook")

      // console.log(this.datamanual,'datamanual');

    })
  }
  gotoDetailsDigital(id) {
    // alert(url)
    // this._homeService.getGeneralNews()
    // .subscribe(result => 
    //   {
    //     this.newsGeneral = result.data
    //     console.log(this.newsGeneral);
    //   }

    //   )
    // window.open(`${url}`, "_blank");

    this.router.navigate(['news-general-details', id]);
  }



  getDigitalUpdate() {
    // this.loading = true
    this._homeService.getDigitalUpdate().subscribe((result: any) => {
      // console.log("digitalUpdateHome",result);
      this.digitalUpdateHomeBanner = result
      this.digitalUpdateHome = result.pr
      this.description = this.digitalUpdateHome[0].description
      this.description1 = this.digitalUpdateHome[1].description
      this.description2 = this.digitalUpdateHome[2].description
      this.description3 = this.digitalUpdateHome[3].description
      this.description4 = this.digitalUpdateHome[4].description

      // console.log(this.digitalUpdateHome,'digitalUpdateHome');
      this.loading = false
    })

  }

  gotoDigitalUpdate() {
    // alert(url)
    // this._homeService.getGeneralNews()
    // .subscribe(result => 
    //   {
    //     this.newsGeneral = result.data
    //     console.log(this.newsGeneral);
    //   }

    //   )
    // window.open(`${url}`, "_blank");

    this.router.navigate(['news-general']);
  }



  getAsaNews() {
    this._homeService.getAsaNews()
      .subscribe(result => {
        this.newsAsa = result.data.slice(0, 6)
        // console.log(this.newsAsa, 'newsAsa');

      })
  }

  getNewsAsds() {
    this._homeService.getNewsAsd()
      .subscribe(result => this.newsAsd = result)
  }

  gotoDetailsNews(id) {
    // alert(id)
    this.router.navigate(['blog-details', id]);
  }


  getActAsd() {

    this._homeService.getActAsd()
      .subscribe(result => {
        this.actAsd = result
        // console.log(result);

      }

      )
  }

  gotoDetailsAct(id) {
    // alert(id)
    this.router.navigate(['services-details', id]);
  }

  getKnowledge() {
    // alert('in')
    this._homeService.getKnowledge()
      .subscribe(result => {

        this.knowledge = result.data;

        this.knowledgeDataPDF = this.knowledge.data;

        let newData = [];
        for (var i = 0; i < result.data.length; i++) {
          if (result.data[i].mediatype == 'เอกสารเพื่อการเรียนรู้ (pdf)') {
            newData.push({
              id: result.data[i].id,
              medianame: result.data[i].medianame,
              mediadetail: result.data[i].mediadetail,
              headlines: result.data[i].headlines,
              mediatype: result.data[i].mediatype,
              created_at: result.data[i].created_at,
              uploadfile: result.data[i].uploadfile,


            })
          }
        }

        this.knowledgeDataPDF = newData.slice(0, 5)

        this.knowledgeDataImage = this.knowledge.data;
        let newData1 = [];
        for (var i = 0; i < result.data.length; i++) {
          if (result.data[i].mediatype == 'สื่อรูปภาพเพื่อการเรียนรู้ (jpg , png)') {
            newData1.push({
              id: result.data[i].id,
              medianame: result.data[i].medianame,
              mediadetail: result.data[i].mediadetail,
              headlines: result.data[i].headlines,
              mediatype: result.data[i].mediatype,
              created_at: result.data[i].created_at,
              uploadfile: result.data[i].uploadfile,
            })
          }
        }

        this.knowledgeDataImage = newData1.slice(0, 2)

        this.knowledgeDataVideo = this.knowledge.data;
        let newData2 = [];
        for (var i = 0; i < result.data.length; i++) {
          if (result.data[i].mediatype == 'สื่อวิดีโอเพื่อการเรียนรู้ (mp4)') {
            newData2.push({
              id: result.data[i].id,
              medianame: result.data[i].medianame,
              mediadetail: result.data[i].mediadetail,
              headlines: result.data[i].headlines,
              mediatype: result.data[i].mediatype,
              created_at: result.data[i].created_at,
              uploadfile: result.data[i].uploadfile,
            })
          }
        }

        this.knowledgeDataVideo = newData2.slice(0, 5)



        // console.log(this.knowledgeDataPDF, 'pdf');
        // console.log(this.knowledgeDataImage, 'Image');
        // console.log(this.knowledgeDataVideo, 'Video');
        // console.log(result);
      }
      )

  }
  openVideo(uploadfile) {
    // alert(uploadfile)


    // console.log(this.knowledgeDataVideo[1].uploadfile,'eoeoe');

    window.open(`http://npcradm.netpracharat.com/mediadetail/${uploadfile}`, "_blank");
  }

  openPDF(uploadfile) {
    // alert(uploadfile)


    // console.log(this.knowledgeDataVideo[1].uploadfile,'eoeoe');

    window.open(`http://npcradm.netpracharat.com/mediadetail/${uploadfile}`, "_blank");
  }

  openImage(uploadfile) {
    // alert(uploadfile)


    // console.log(this.knowledgeDataVideo[1].uploadfile,'eoeoe');

    window.open(`http://npcradm.netpracharat.com/mediadetail/${uploadfile}`, "_blank");
  }



  openDE() {
    window.open(`https://www.mdes.go.th/`, "_blank");
  }
  openTMD() {
    window.open(`https://www.tmd.go.th/index.php`, "_blank");
  }
  openDEPA() {
    window.open(`https://www.depa.or.th/th/home`, "_blank");
  }
  openNSO() {
    window.open(`hhttp://www.nso.go.th/sites/2014`, "_blank");
  }
  openONDE() {
    window.open(`https://www.onde.go.th/view/1/%E0%B8%AB%E0%B8%99%E0%B9%89%E0%B8%B2%E0%B9%81%E0%B8%A3%E0%B8%81/TH-TH`, "_blank");
  }
  openTOT() {
    window.open(`https://www.ntplc.co.th/`, "_blank");
  }
  openCAT() {
    window.open(`https://www.cattelecom.com/cat/index.php`, "_blank");
  }
  openthpost() {
    window.open(`https://www.thailandpost.co.th/th/index/`, "_blank");
  }
  openETDA() {
    window.open(`https://www.etda.or.th/th/`, "_blank");
  }

  openWeb(url) {
    window.open(url, "_blank");
  }



  openManual_1(file) {
    // window.open(`https://drive.google.com/file/d/1lAVA1344Hc34rxfwsM9_fOalDM0PiGIT/view?usp=sharing`, "_blank");
    window.open(`${file}`, "_blank");
  }


  openManual_2() {
    window.open(`https://drive.google.com/file/d/1tdR2ChsOtr_SF2qupfU06BRF8giJymXZ/view?usp=sharing`, "_blank");
  }


  openManual_4() {
    window.open(`http://ebook04.sure.guru/`, "_blank");
  }



  openCourse_2() {
    window.open(`https://drive.google.com/file/d/1OA1eEO3LhcZdeAY2RuWbEtVRzYeeDWs2/view?usp=sharing`, "_blank");
  }
  openCourse_3() {
    window.open(`https://drive.google.com/file/d/1XP3vCo0w-Xb85ETakkeqhGcjU2mZdR7y/view?usp=sharing`, "_blank");
  }



  openPT_1() {
    window.open(`https://drive.google.com/file/d/19M3qaDLjcC8lCHbdYt0gin3bFyRmmX9s/view`, "_blank");
  }
  openPT_7() {
    window.open(`https://lms.thaimooc.org/courses/course-v1:OCSC+OCSC008+2019/about`, "_blank");
  }
  openPT_8() {
    window.open(`https://thaimooc.org/course/THAIMOOC%3A%3Acourse--v1%3ADEPA-DEPA001-2018`, "_blank");




  }
  openEL_1() {
    window.open(`https://www.coursesquare.co/course/courseInfo/The-Fundamentals-of-Smart-City`, "_blank");
  }



  //////////////////////////// open other services  ////////////////////////////


  openSure() {
    window.open(`https://www.youtube.com/channel/UC5Bd7izFr5x3jNL0peJpmaA`, "_blank");
  }

  openFacebook() {
    window.open(`https://www.facebook.com/TDV.Thailand/`, "_blank");
  }
  openFakenews() {
    window.open(`https://www.antifakenewscenter.com/`, "_blank");
  }

  openNPR() {
    window.open(`https://npcr.netpracharat.com/AboutNetpracharat/Village.aspx`, "_blank");
  }



  openResgis() {
    window.open(`http://tdv.netpracharat.com/admin/registerdigital`, "_blank");
  }

}
