import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HomeService } from 'src/app/services/home.service';

@Component({
  selector: 'app-checklist-training',
  templateUrl: './checklist-training.component.html',
  styleUrls: ['./checklist-training.component.scss']
})
export class ChecklistTrainingComponent implements OnInit {
  dtOptions: any = {};
  loading = true;
  loading2 = true;
  loading3 = true;
  loading4 = true;
  list_trainingUser: any = []
  list_trainingUser2: any = []
  constructor(private _homeService: HomeService, private router: Router) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      columnDefs: [
        {
          orderable: false
        }
      ],
      "language": {
        "lengthMenu": "แสดง  _MENU_  รายการ",
        "search": "ค้นหา"
        ,
        "info": "แสดง _PAGE_ ของ _PAGES_ รายการ",
        "infoEmpty": "แสดง 0 ของ 0 รายการ",
        "zeroRecords": "ไม่พบข้อมูล",
        "paginate": {
          "first": "หน้าแรก",
          "last": "หน้าสุดท้าย",
          "next": "ต่อไป",
          "previous": "ย้อนกลับ"
        },
      }
    };
    // this.getListTrainings(18, 1)
    this.getListTrainings(20, 2)
    // this.getListTrainings()
  }


  getListTrainings(genaration: any, number: any) {
    // this.loading = false
    this.loading = true
    this.loading2 = true
    this.loading3 = true
    this.loading4 = true

    if (number == 1) {
      this._homeService.getListTrainingUser(genaration).subscribe((result: any) => {
        // console.log(result);
        
        this.list_trainingUser = result
        this.loading = false
        this.loading2 = true
        this.loading3 = true
        this.loading4 = true
      });
    }
    else if (number == 2) {
      this._homeService.getListTrainingUser(genaration).subscribe((result2: any) => {
        // console.log(result2);
        this.list_trainingUser = result2
        this.loading = true
        this.loading2 = false
        this.loading3 = true
        this.loading4 = true

      });
    }
    else if (number == 3) {
      this._homeService.getListTrainingUser(genaration).subscribe((result3: any) => {
        // console.log(result3);
        this.list_trainingUser = result3
        this.loading = true
        this.loading2 = true
        this.loading3 = false
        this.loading4 = true
      });
    }
    else {
      this._homeService.getListTrainingUser(genaration).subscribe((result4: any) => {
        // console.log(result4);
        this.list_trainingUser = result4
        this.loading = true
        this.loading2 = true
        this.loading3 = true
        this.loading4 = false
      });
    }
  }


}
