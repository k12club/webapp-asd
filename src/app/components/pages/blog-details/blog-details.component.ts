import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/services/home.service';
import { NewsAsd } from 'src/app/models/newsAsd';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.scss']
})
export class BlogDetailsComponent implements OnInit {

  newsAsd: NewsAsd[] = []
  id : any;
  data : any;
  dataNews : any;
  loading = false;
  loading1 = false;

  constructor(
    private _homeService: HomeService,
    private activatedRoute: ActivatedRoute,
   

    ){
      this.id = activatedRoute.snapshot.paramMap.get('id')
     }

  ngOnInit(): void {
    // alert(this.id)
    this.getDetailsNews()
    //console.log(Object.prototype.toString.call(this.data),'this.data')
  //  this.getNewsAsds()
  this.getActAsdDetail()
  }
  


  getActAsdDetail() {
    this._homeService.getActAsdDetail(this.id)
      .subscribe(result => {
        this.data = result;
        this.loading1 = true;
      })
  }

  

  getDetailsNews() {
    this._homeService.getDetailsNews(this.id)
      .subscribe(result => {
        this.dataNews = result;
        this.loading = true;
      })
  }



  
}