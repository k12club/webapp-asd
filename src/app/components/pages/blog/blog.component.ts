import { Component, OnInit } from '@angular/core';
import { NewsAsd } from 'src/app/models/newsAsd';
import { HomeService } from 'src/app/services/home.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  newsAsd: NewsAsd[] = []
  newsAsa: any;
  data : any;
  loading1 = false;
  id : any;

  constructor( 
    private _homeService: HomeService,
    private router: Router
    ) 
    
    { }
  ngOnInit(): void {
    this.getNewsAsds()
    this.getAsaNews()
  
  }

  getNewsAsds() {
    this._homeService.getNewsAsd()
      .subscribe(result => this.newsAsd = result)
      // console.log(this.newsAsd,'newsAsd');
      
  }  

  getAsaNews() {
    this._homeService.getAsaNews()
      .subscribe(result => 
        {
          this.newsAsa = result.data
          // console.log(this.newsAsa, 'newsAsa');
          
        })
  }



  gotoDetailsNews(id){
    // alert(id)
    this.router.navigate(['blog-details', id]);
  }

   getActAsdDetail(id) {
    // this._homeService.getActAsdDetail(this.id)
    //   .subscribe(result => {
    //     this.data = result;
    //     this.loading1 = true;
    //   })
    this.router.navigate(['services-details', id]);
  }

  

}