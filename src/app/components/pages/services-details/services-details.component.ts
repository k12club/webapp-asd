import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/services/home.service';
import { NewsAsd } from 'src/app/models/newsAsd';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-services-details',
  templateUrl: './services-details.component.html',
  styleUrls: ['./services-details.component.scss']
})
export class ServicesDetailsComponent implements OnInit {
 actAsd: any
  id : any;
  data : any;
  loading = false;

  constructor(
    private _homeService: HomeService,
    private activatedRoute: ActivatedRoute,
   

    ){
      this.id = activatedRoute.snapshot.paramMap.get('id')
     }

  ngOnInit(): void {
    // alert(this.id)
    this.getActAsdDetail()
    //console.log(Object.prototype.toString.call(this.data),'this.data')
  //  this.getNewsAsds()
  }
  // getDetailsNews() {
  //   this._homeService.getDetailsNews(this.id)
  //     .subscribe(result => {
  //       this.data = result;
  //       this.loading = true;
  //     })
  // }

  getActAsdDetail() {
    this._homeService.getActAsdDetail(this.id)
      .subscribe(result => {
        this.data = result;
        this.loading = true;
        // console.log(this.data);
        
      })
  }

 

  

}