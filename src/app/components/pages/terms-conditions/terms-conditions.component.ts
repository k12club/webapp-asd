import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-terms-conditions',
  templateUrl: './terms-conditions.component.html',
  styleUrls: ['./terms-conditions.component.scss']
})
export class TermsConditionsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  openDE() {    
    window.open(`https://www.mdes.go.th/`, "_blank");
  }
  openFacebook() {    
    window.open(`https://www.facebook.com/อาสาสมัครดิจิทัล-105256638019733`, "_blank");
  }

  openLineOpenChat() {    
    window.open(`https://line.me/ti/g2/xrwwjSFm91xbNolocPCh5Q?utm_source=invitation&utm_medium=link_copy&utm_campaign=default`, "_blank");
  }
}
