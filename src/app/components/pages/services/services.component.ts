import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HomeService } from 'src/app/services/home.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  actAsd : any;
  digitalNews : any;

  constructor(
    private _homeService: HomeService,
    private router: Router
  ) { }
 
  ngOnInit(): void {
    this.getDigitalNews()
  }

  // getActAsd() {

  //   this._homeService.getActAsd()
  //     .subscribe(result => 
        
  //       {
  //         this.actAsd = result
  //       // console.log(result);
        
  //       }
        
  //       )
  // }  

  getDigitalNews() {

    this._homeService.getDigitalNews()
      .subscribe(result => 
        
        {
          this.digitalNews = result
        // console.log(this.digitalNews);
        
        }
        
        )
  }  

  

  gotoDetailsAct(id){
    // alert(id)
    this.router.navigate(['blog-details', id]);
  }

}
