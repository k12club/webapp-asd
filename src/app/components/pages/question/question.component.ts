import { Component, OnInit,ViewChild, ElementRef,AfterViewInit  } from '@angular/core';
import { AuthenticationService } from '../../../services/auth/_services/authentication.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
  FormArray
} from '@angular/forms';
import { Router } from '@angular/router';
import { HomeService } from 'src/app/services/home.service';
declare let $: any;


@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit,AfterViewInit {
  @ViewChild('exampleModal') modalElement: ElementRef;
  @ViewChild('testScoreModal') testScoreModalElement: ElementRef;
  user: any
  constructor(
    private authenticationService: AuthenticationService,
    private fb: FormBuilder,
    private homeService: HomeService,
    private router: Router,

  ) { 
   this.user = this.authenticationService.currentUserValue?.data
  }
 
  Assesment: FormGroup
  form: FormGroup
  loading = true
  question : any =[]
  subquestions : any =[]
  totalScores: number[] = [];
  answeredQuestionsCount: number = 0;
  finalTotalScore: number = 0;

  ngAfterViewInit() {
    // Now you can safely access the modalElement.nativeElement
    this.closeModal();
   
  }

  get f() {
    return this.form.controls;
  }
  ngOnInit(): void {
    $.getScript('../assets/js/custom.js');
    this.getAssessment()
    this.form = this.fb.group({
      user_id: new FormControl('', [Validators.required]),
      totalScore:  new FormControl('')
    });
  }
 
  handleOptionChange(questionIndex: number, newValue: any, event: any) {
    while (this.totalScores.length <= questionIndex) {
      this.totalScores.push(0); 
    }
    if (newValue === 1) { 
      if (this.totalScores[questionIndex] === 0 || this.totalScores[questionIndex] === undefined) {
        this.totalScores[questionIndex] = 1;
      }
    } else {
      this.totalScores[questionIndex] = 0;
    }
    // console.log(`Total Score for Question ${questionIndex}: ${this.totalScores[questionIndex]}`);
    this.finalTotalScore = this.totalScores.reduce((sum, score) => sum + score, 0);
    return this.finalTotalScore;
  }

  getAssessment(){
    this.loading = true
    this.homeService.getAssessment().subscribe((result:any) => {
      this.question = result
      this.subquestions = result
      // console.log(this.subquestions);      
      this.loading = false
      $('.loader').fadeOut('slow');
    })
  }

  submitAssessment() {
    this.form.patchValue({
      user_id: this.user?.id,
      totalScore: this.finalTotalScore
    });
    
    const modal:any = this.testScoreModalElement.nativeElement;
    $(modal).modal('show'); 

    this.homeService.sendAssessment(this.form.value).subscribe((res)=>{
      // this.router.navigate(['home']); 
    })
   
    // this.closeModal(); 
  }
  closeModal() {
    const modal:any = this.modalElement.nativeElement;
    $(modal).modal('hide'); 
  }

  testScoreModalcloseModal() {
    const modal:any = this.testScoreModalElement.nativeElement;
    $(modal).modal('hide'); 
    if(this.finalTotalScore >= 12){
      this.router.navigate(['home']); 
    }
  
  }

}
