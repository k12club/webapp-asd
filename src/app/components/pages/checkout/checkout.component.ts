import { Component, OnInit, } from '@angular/core';
import {FormControl, Validators} from '@angular/forms'
import { HomeService } from 'src/app/services/home.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  prefix: any = ["นาย", "นาง" ,"นางสาว"]
  gender: any = ["ชาย", "หญิง"]
  addWorkForm: any
  genderForm: any
  prefixstatus= false
  genderstatus= false
  submitted =false
  submittedGender =false
  province: any =[]
  district:any =[]
  provinces:any
  amphoes:any
  fb: any;
  forbiddenNames: any;
  status: boolean;
  checkTel: any;
 
 

  
  constructor(
    private _homeService: HomeService,
  ) { }

  ngOnInit(): void {
    

   this.getProvince()
 
  }

  AddWorkForm() {
    this.addWorkForm = this.fb.group({
      "firstName": new FormControl(null, [Validators.required]),
      'lastName': new FormControl(null, [Validators.required]),
      'zipcode': new FormControl(null, [Validators.required]),
      // 'email': new FormControl(null, [Validators.required,
      // Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,4}$")], this.forbiddenNames.bind(this)),
      // 'tel': new FormControl(null, [Validators.required, Validators.pattern(/^-?([0-9]\d*)?$/)], this.checkTel.bind(this)),
      'province': new FormControl(null, [Validators.required]),
      'amphoe': new FormControl(null, [Validators.required]),
      'district': new FormControl(null, [Validators.required]),
      'career': new FormControl(null),  
      'birthday': new FormControl(null),
      'address': new FormControl(null, [Validators.required]),
      'lineID': new FormControl(null),
      'Langage': new FormControl(null),
      'prefixtext': new FormControl(null),
      'prefix': new FormControl(null),
  
    })}

  getProvince() {
    this._homeService.getProvince()
      .subscribe(result => 
        {
          this.province = result
          // console.log(this.province, 'province');

        
          
        })
  }
  




  onChangamphoe(provinceId) {

    alert( this.province.filter(result=>{
      return result.province_code == provinceId.target.value
    })[0].province)

    this.addWorkForm.patchValue({
      "province": this.province.filter(result => {
        return result.province_code == provinceId.target.value
      })[0].province
    })
    // alert(JSON.stringify(this.addWorkForm))
    // console.log("test", provinceId.target)

    this._homeService.getAmphoe(provinceId.target.value).subscribe(results => {
      this.amphoes = results
      // console.log("อำเภอ", this.amphoes);

    })
  
  }

 


  onChangeprefix(prefix) {
    // console.log(f.prefixtext.errorse);
    {

    this.addWorkForm.get('prefix').valueChanges.subscribe(val => {
      // alert(val)
      if (val == "อื่นๆ") {
          this.addWorkForm.controls['prefixtext'].setValidators([Validators.required]);
          this.addWorkForm.controls['prefixtext'].updateValueAndValidity();
          this.prefixstatus = true
      } else {

          this.addWorkForm.controls['prefixtext'].clearValidators();
          this.addWorkForm.controls['prefixtext'].updateValueAndValidity();
          this.addWorkForm.patchValue({
            "prefixtext": null
          })

          this.prefixstatus = false
      }
  });
}}


// onChangeGender(gender) {
//   // console.log(f.prefixtext.errorse);
//   {

//   this.genderForm.get('gender').valueChanges.subscribe(val => {
//     // alert(val)
//     if (val == "อื่นๆ") {
//         this.genderForm.controls['gendertext'].setValidators([Validators.required]);
//         this.genderForm.controls['gendertext'].updateValueAndValidity();
//         this.genderstatus = false
//     } else {

//         this.genderForm.controls['gendertext'].clearValidators();
//         this.genderForm.controls['gendertext'].updateValueAndValidity();
//         this.genderForm.patchValue({
//           "gendertext": null
//         })

//         this.genderstatus = false
//     }
// });
// }}





}
