import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {


  }

  openFacebook() {    
    window.open(`https://www.facebook.com/อาสาสมัครดิจิทัล-105256638019733`, "_blank");
  }
  openLineOpenChat() {    
    window.open(`https://line.me/ti/g2/xrwwjSFm91xbNolocPCh5Q?utm_source=invitation&utm_medium=link_copy&utm_campaign=default`, "_blank");
  }


}
