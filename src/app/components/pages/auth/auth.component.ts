import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { HomeService } from 'src/app/services/home.service';
import { jwtDecode } from "jwt-decode"
import { AuthenticationService } from 'src/app/services/auth/_services';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';
@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  code : String
  returnUrl : String
  constructor(private route: ActivatedRoute , 
    private router: Router,
    private authenticationService: AuthenticationService,
    private homeService : HomeService) { }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.route.queryParams.subscribe(params => {
      this.code = params['code'];
      console.log(this.code)
        // alert('in')
      this.homeService.ThaiIdLogin(this.code , "web").subscribe(result => {
        const decoded : any = jwtDecode(result.data.id_token);
        // console.log(Z)
        let words = decoded.address.formatted.split(' ');
        let wordslice = words.slice(-3)
        // wor
        let province =  wordslice[2].replace("จ." , "")
        let district =  wordslice[1].replace("เขต" , "").replace("อ." , "") 
        let subdistrict = wordslice[0].replace("แขวง" , "").replace("ต." , "")
        // console.log(province)
        // console.log(district)
        // console.log(subdistrict)
        const formData = {
          IDcard : decoded.pid ,
          province : province ,
          district : district , 
          subdistrict : subdistrict ,
          name :  decoded.given_name,
          lastname : decoded.family_name  ,
          gender : decoded.gender ,
          prefix : decoded.titleTh ,
          address : decoded.address.formatted ,
          birthdate : decoded.birthdate
         }

        this.homeService.checkThaiID(formData).subscribe(result => {
        // console.log(result)
          this.authenticationService.login(result)
          this.router.navigate([this.returnUrl])
        })
        // console.log(result.data)
      })
      // console.log(this.code)
    });
 
  }


}
