import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealtyComponent } from './healty.component';

describe('HealtyComponent', () => {
  let component: HealtyComponent;
  let fixture: ComponentFixture<HealtyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealtyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealtyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
