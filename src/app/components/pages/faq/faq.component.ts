import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/services/home.service';

import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  loading1 = false;
  loading2 = false;
  loading3 = false;
  loading4 = false;
  loading5 = false;


  dataGen1: any;
  dataGen2: any;
  dataGen3: any;
  dataGen4: any;
  dataGen5: any;
  province1: any;
  dataCheck: any;

  getfile: any[] = [
    {
      id: 1, name: "ดาวน์โหลดเอกสารข้อมูลแกนนำรุ่นที่ 1", source: 'ทำเนียบรุ่น_รุ่น1.pdf',
    },
    {
      id: 2, name: "ดาวน์โหลดเอกสารข้อมูลแกนนำรุ่นที่ 2", source: 'ทำเนียบรุ่น_รุ่น2.pdf',
    },
    {
      id: 3, name: "ดาวน์โหลดเอกสารข้อมูลแกนนำรุ่นที่ 3", source: 'ทำเนียบรุ่น_รุ่น3.pdf',
    },
    {
      id: 4, name: "ดาวน์โหลดเอกสารข้อมูลแกนนำรุ่นที่ 4", source: 'ทำเนียบรุ่น_รุ่น4.pdf',
    },
    {
      id: 5, name: "ดาวน์โหลดเอกสารข้อมูลแกนนำรุ่นที่ 5", source: 'ทำเนียบรุ่น_รุ่น5.pdf',
    }
  ]


  getDataGen: any = [
    { id: 1 },
    { id: 2 },
    { id: 3 },
    { id: 4 },
    { id: 5 },
  ];


  id: any;

  constructor(
    private _homeService: HomeService,
    private activatedRoute: ActivatedRoute,

  ) {

  }

  ngOnInit(): void {

    this.getGeneration1()
    // this.getSendWork()

    // this.getGeneration2()
    // this.getGeneration3()
    // this.getGeneration4()
    // this.getGeneration5()

  }

  // getGeneration1() {
  //   // alert(id)
  //   this._homeService.getGeneration1()
  //     .subscribe(result => {
  //       this.dataGen1 = result;
  //       this.loading1 = true;
  //       console.log(this.dataGen1)
  //     })

  // }


  openPDF(source) {
    // alert(uploadfile)


    // console.log(this.knowledgeDataVideo[1].uploadfile,'eoeoe');
    window.open("assets/pdf/" + source);
    // window.open(`${id}`, "_blank");
  }



  scrollTo(elementName) {
    let el = document.getElementById(elementName);
    window.scrollTo({ left: 0, top: el.offsetTop - 40, behavior: 'smooth' });
  }



  getGeneration1() {
    // alert(id)
    this._homeService.getGeneration1()
      .subscribe(result => {

        // this.dataGen1 = result.filter((result)=> result.generation_id=="1");
        this.dataGen1 = result;


        this.loading1 = true;

        // console.log(this.dataGen1, 'dataGen1')


      })

  }

  getGeneration2(id) {
    // alert(id)
    this._homeService.getGeneration2(id)
      .subscribe(result => {
        this.dataGen1 = result;
        this.loading2 = true;
        // console.log(this.dataGen1,'anotherGen')
      })

  }



  getSendWork() {
    this._homeService.getSendWork()
      .subscribe(result => {
        this.dataCheck = result;
        this.loading3 = true;
        // console.log(this.dataCheck, 'dataCheck')
      })
  }




}
