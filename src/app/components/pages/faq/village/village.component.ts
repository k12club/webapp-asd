import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/services/home.service';

import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-village',
  templateUrl: './village.component.html',
  styleUrls: ['./village.component.scss']
})
export class VillageComponent implements OnInit {
  loading1 = false;
  loading2 = false;
  loading3 = false;
  loading4 = false;
  loading5 = false;


  dataGen1: any = [];
  dataGen2: any;
  dataGen3: any;
  dataGen4: any;
  dataGen5: any;
  province1: any;
  dataCheck: any;

  


  getDataGen: any = [
    { id: 6 , id_village: 1},
    { id: 7,id_village: 2 },
    { id: 8, id_village: 3 },
    { id: 9, id_village: 4 },
  ];

  id: any;

  constructor(
    private _homeService: HomeService,
    private activatedRoute: ActivatedRoute,

  ) {

  }

  ngOnInit(): void {

    this.getGeneration6()
  
  }



  scrollTo(elementName) {
    let el = document.getElementById(elementName);
    window.scrollTo({ left: 0, top: el.offsetTop - 40, behavior: 'smooth' });
  }



  getGeneration6() {
    // alert(id)
    this._homeService.getGeneration6()
      .subscribe(result => {
        // this.dataGen1 = result.filter((result)=> result.generation_id=="3");
        this.dataGen1 = result.filter((result)=> result.users.length != 0);
        // console.log(this.dataGen1, 'dataGen1')
      })
  }
  getGeneration_Another(id) {
    // alert(id)
    this._homeService.getGeneration_Another(id)
      .subscribe(result => {
        this.dataGen1 = result.filter((result)=> result.users.length != 0);
        //this.loading2 = true;
        //  console.log(this.dataGen1,'anotherGen')
      })

  }

}
