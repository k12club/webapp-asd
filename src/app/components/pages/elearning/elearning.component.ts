import { Component, OnInit } from "@angular/core";
import { HomeService } from "src/app/services/home.service";
import { Router } from "@angular/router";
declare let $: any;

@Component({
  selector: "app-elearning",
  templateUrl: "./elearning.component.html",
  styleUrls: ["./elearning.component.scss"],
})
export class ElearningComponent implements OnInit {
  dataCourse: any = [];
  dataCourse_elective: any = [];
  dataCourse_all: any = [];
  // loading = true
  loading1 = true
  loading2 = true
  loading3 = true
  loading4 = true

  // public data_course: {data: string} = {  data : this.dataCourse };

  constructor(private _homeService: HomeService, private router: Router) { }

  ngOnInit(): void {
    // this.getKnowledge()
    // $('.loader').fadeIn('slow');
    $.getScript('../assets/js/custom.js');
    // this.getCourse_core();
    // this.getCourse_elective()
    // this.getCourseTPQI()
    // this.getCourseGCC()
    this.getCourse(1)
    $('.loader').fadeOut('slow');
  }


  // getCourse_all() {
  //   // this.loading = false
  //   this.loading = true
  //     this._homeService.getCourse().subscribe((result: any) => {
  //         // console.log("Manual",result);
  //         this.dataCourse_all = result
  //         this.loading = false
  //         // this.loading = true
  //         // console.log(this.dataCourse_all, "data all");
  //     });
  // }

  getCourse(number) {
    if (number == 1) {

      this.loading2 = true
      this.loading3 = true
      this.loading4 = true

      this._homeService.getCourse().subscribe((result: any) => {
        // console.log("Manual",result);
        this.dataCourse = result.filter((data) => data.type == "core");
        this.loading1 = false
        // console.log(this.dataCourse, "datamanual");
      });
    }
    else if (number == 2) {
      this.loading1 = true
      this.loading3 = true
      this.loading4 = true
      this._homeService.getCourse().subscribe((result: any) => {
        // console.log("Manual",result);
        this.dataCourse_elective = result.filter((data) => data.type == "elective");
        this.loading2 = false
        // console.log(this.dataCourse_elective, "dataCourse_elective");
      });


    }
    else if (number == 3) {
      this.loading1 = true
      this.loading2 = true
      this.loading4 = true
      this._homeService.getDCC().subscribe((result: any) => {
        // console.log("datatpqi",result.data);
        this.datagcc = result.data
        this.loading3 = false

      });

    }
    else {
      this.loading1 = true
      this.loading2 = true
      this.loading3 = true

      this._homeService.getTPQI().subscribe((result: any) => {
        // console.log("datagcc",result.data);
        this.datatpqi = result.data
        this.loading4 = false
      });
    }


  }

  getCourse_elective() {
    // this.loading2 = true
    // this.loading1 = true

  }

  datatpqi: any
  getCourseTPQI() {
    // this.loading1 = true

  }
  datagcc: any
  getCourseGCC() {
    // this.loading1 = true

  }

  openManual(file) {
    if (file == null) {
      alert("รอเปิดหลักสูตร")
    }
    else {
      window.open(`${file}`, "_blank");
      // window.open(`https://lms.thaimooc.org/courses/course-v1:STOU+STOU005+2017/about`, "_blank");
    }

  }


  openManual_1(file) {
    if (file == null) {
      alert("รอเปิดหลักสูตร")
    }
    else {
      window.open(`${file}`, "_blank");
      // window.open(`https://lms.thaimooc.org/courses/course-v1:STOU+STOU005+2017/about`, "_blank");
    }

  }


  openManual_2(file2) {
    if (file2 == null) {
      alert("รอเปิดหลักสูตร")
    }
    else {
      window.open(`${file2}`, "_blank");
    }

  }
  gotoDetailElearning(id) {
    // alert(id)
    this.router.navigate(['/elearning-details/', id]);
    // this.router.navigate(['elearning-details/'],id );
  }
}
