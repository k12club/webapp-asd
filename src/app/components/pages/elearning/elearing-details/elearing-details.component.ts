import { Component, Input, OnInit } from '@angular/core';
import { HomeService } from 'src/app/services/home.service';
import { NewsAsd } from 'src/app/models/newsAsd';
import { ActivatedRoute } from '@angular/router';
import { Router } from "@angular/router";
@Component({
  selector: 'app-elearing-details',
  templateUrl: './elearing-details.component.html',
  styleUrls: ['./elearing-details.component.scss']
})
export class ElearingDetailsComponent implements OnInit {
  
  id : any;
  dataDetails : any;
  dataVideo :any = [];
  loading = true;
  loadingVideo = true;

  description: any;

  sub:any
  page: any

  constructor(
    private _homeService: HomeService,
    private activatedRoute: ActivatedRoute,
    private router: Router

    ){
      this.id = this.activatedRoute.snapshot.paramMap.get('id')
     }

  ngOnInit() {
    this.getDetailsCourse()
    // console.log("getCurrentNavigation",this.data);
    // this.dataDetails = this.data
    
   
    // this.data = this.activatedRoute.params.subscribe( (params : any) => {
    //   this.dataDetails = params;
    //   console.log('dataDetails',this.dataDetails);
  
    //   });
    //   this.loading = true;
  }
  getDetailsCourse(){
   
    this._homeService.getDetailsCourse(this.id).subscribe((result: any) => {
        console.log("dataDetails =>",result);
        this.dataDetails = result
        this.dataVideo = result.videos
        this.description = result.description
        this.loading = false
        // console.log(this.dataCourse, "datamanual");
    });
 
  }

  viewVideo(video , i){
    if( i == 0){
      window.open(`https://tdv.netpracharat.com:5000/uploads/vedio/${video}`);
    }
    else{
      window.open(`${video}`);
    }
  }

  downloadDocument(){
    window.open(`https://drive.google.com/drive/folders/1Vk0TGghCUK6mC2NihJb9NRvfEzc7gqiN?usp=sharing`)
  }
 
}
