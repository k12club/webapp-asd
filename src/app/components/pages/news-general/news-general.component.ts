import { Component, OnInit } from '@angular/core';
import { NewsAsd } from 'src/app/models/newsAsd';
import { HomeService } from 'src/app/services/home.service';
import { Router } from '@angular/router';
// import { Meta } from '@angular/platform-browser';
import { Title, Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-news-general',
  templateUrl: './news-general.component.html',
  styleUrls: ['./news-general.component.scss']
})
export class NewsGeneralComponent implements OnInit {

  newsAsd: NewsAsd[] = []
  newsGeneral: any;
  DigitalUpdate: any=[]
  description: any;
  loading = false;
  // getNews : any;

  
  private titleMeta: string = "og:title";
  private descriptionMeta: string = "og:description";
  private imageMeta: string = "og:image";
  private secureImageMeta: string = "og:image:secure_url";
  constructor(
    private _homeService: HomeService,
    private router: Router,
    private metaService: Meta
  ) { }

  ngOnInit(): void {
    // this.getNewsAsds()
    // this.getGeneralNews()
    this.getDigitalUpdate()
    // this.setFacebookTags();

  }
  // private setTags(tags): void {
  //   tags.forEach(siteTag => {
  //     this.metaService.updateTag({ property: siteTag.titleMeta, content: siteTag.descriptionMeta });
  //   });
  // }
  // public setFacebookTags(): void {
  //   // var imageUrl = `https://images.codinghub.net/${image}`;
  //   var tags = [
  //     this.titleMeta= this.DigitalUpdate.name,
  //     this.descriptionMeta =   this.DigitalUpdate.description,
      
  //   ];
  //   this.setTags(tags);
  // }
  // getNewsAsds() {
  //   this._homeService.getNewsAsd()
  //     .subscribe(result => this.newsAsd = result)
  //     console.log(this.newsAsd);
  // }  

  getGeneralNews() {
    this._homeService.getGeneralNews()
      .subscribe(result => {
        this.newsGeneral = result.data
        // console.log(this.newsGeneral);
      }

      )
  }


  getDigitalUpdate() {
    this._homeService.getDigitalUpdate()
      .subscribe(result => {
        // console.log(result);
        
        this.DigitalUpdate = result;
        // this.description =  this.DigitalUpdate.description
        this.loading = true;
      }

      )
  }


  gotoDetailsNews(id) {
    // alert(url)
    // this._homeService.getGeneralNews()
    // .subscribe(result => 
    //   {
    //     this.newsGeneral = result.data
    //     console.log(this.newsGeneral);
    //   }

    //   )
    // window.open(`${url}`, "_blank");

    this.router.navigate(['news-general-details', id]);
  }

}
