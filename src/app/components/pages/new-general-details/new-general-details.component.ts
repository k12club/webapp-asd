import { Component, OnInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { FacebookService, InitParams, UIParams, UIResponse } from 'ngx-facebook';
import { HomeService } from 'src/app/services/home.service';

@Component({
  selector: 'app-new-general-details',
  templateUrl: './new-general-details.component.html',
  styleUrls: ['./new-general-details.component.scss']
})
export class NewGeneralDetailsComponent implements OnInit {
  id: any;
  share: any;
  data: any;
  dataDigital: any;
  loading = false;

  description: any;

  constructor(
    private _homeService: HomeService,
    private activatedRoute: ActivatedRoute,
    private metaService: Meta,
    private fb: FacebookService

  ) {
    this.id = activatedRoute.snapshot.paramMap.get('id')
    this.share = activatedRoute.snapshot.queryParams['share']
    // console.log(this.share, 'share');

    const initParams: InitParams = {
      appId: '3708762266016134',
      version: 'v3.0'
    };

    fb.init(initParams);
  }

  ngOnInit(): void {
    this.getDetailsDigitalUpdate()
    // this.share == 'facebook' ? this.shareWithOpenGraphActions() : null
  }

  getDetailsDigitalUpdate() {
    // https://tdv.netpracharat.com:5000/uploads/PR/607235ee54fc0.jpeg
    // alert('1')
    this._homeService.getDetailsDigitalUpdate(this.id)
      .subscribe(result => {
        this.dataDigital = result;
        this.description = this.dataDigital.description
        // console.log(this.dataDigital);
        this.loading = true;
      })
   


  }

  shareWithOpenGraphActions() {
    const params: UIParams = {
      method: 'share_open_graph',
      action_type: 'og.shares',
      action_properties: JSON.stringify({
        object: {
          'og:url': `https://tdv.netpracharat.com/news-general-details/${this.id}`,
          'og:title': 'หัวข้ออันบั๊กเอ้ก',
          'og:description': 'ข้อความยาว ๆ',
          'og:image': 'https://tdv.netpracharat.com:5000/uploads/PR/607235ee54fc0.jpeg'
        }
      })
    };
    this.fb.ui(params)
      .then((res: UIResponse) => console.log(res))
      .catch((e: any) => console.error(e));
  }
}
