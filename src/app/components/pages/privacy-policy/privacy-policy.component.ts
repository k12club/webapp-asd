import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss']
})
export class PrivacyPolicyComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  openFacebook() {    
    window.open(`https://www.facebook.com/อาสาสมัครดิจิทัล-105256638019733`, "_blank");
  }

 
  openDE() {    
    window.open(`https://www.mdes.go.th/`, "_blank");
  }

  openGCC() {    
    window.open(`http://www.gcc.go.th/`, "_blank");
  }

  openNPCR() {    
    window.open(`https://npcr.netpracharat.com/AboutNetpracharat/Village.aspx`, "_blank");
  }

  openAnti() {    
    window.open(`https://www.antifakenewscenter.com/`, "_blank");
  }

  openTMD() {    
    window.open(`https://www.tmd.go.th/index.php`, "_blank");
  }

  openTCSD() {    
    window.open(`https://tcsd.go.th`, "_blank");
  }
  openmoicovid() {    
    window.open(`http://www.moicovid.com/ข้อมูลสำคัญ-จังหวัด/`, "_blank");
  }
  
  openNSO() {    
    window.open(`https://gis.nso.go.th/`, "_blank");
  }
  
  openTPM() {    
    window.open(`https://www.thailandpostmart.com/`, "_blank");
  }
  
  openonde() {    
    window.open(`https://dcc.onde.go.th/home`, "_blank");
  }
  openetda() {    
    window.open(`https://ifbl.etda.or.th/`, "_blank");
  }
  opendefund() {    
    window.open(`https://www.defund.in.th/`, "_blank");
  }
  opengdcc() {    
    window.open(`https://gdcc.onde.go.th/`, "_blank");
  }
  opentot() {    
    window.open(`https://www.tot.co.th/eservice`, "_blank");
  }
  opencat() {    
    window.open(`https://www.cat1322.com/eservice`, "_blank");
  }
  openthaicert() {    
    window.open(`https://www.thaicert.or.th/`, "_blank");
  }
  openocc() {    
    window.open(`https://www.1212occ.com/`, "_blank");
  }
  



  openSure() {    
    window.open(`https://www.youtube.com/channel/UC5Bd7izFr5x3jNL0peJpmaA`, "_blank");
  }
  openFakenews() {    
    window.open(`https://www.antifakenewscenter.com/`, "_blank");
  }

  openNPR() {    
    window.open(`https://npcr.netpracharat.com/AboutNetpracharat/Village.aspx`, "_blank");
  }


  openAppNPR() {    
    window.open(`https://apps.apple.com/th/app/%E0%B9%80%E0%B8%84%E0%B8%A3-%E0%B8%AD%E0%B8%82-%E0%B8%B2%E0%B8%A2%E0%B9%80%E0%B8%99-%E0%B8%95%E0%B8%AD%E0%B8%B2%E0%B8%AA%E0%B8%B2%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%8A%E0%B8%B2%E0%B8%A3-%E0%B8%90/id1441632402`, "_blank");
  }
  openAppTMD() {    
    window.open(`https://apps.apple.com/th/app/tmd-smart-sim/id1289127747`, "_blank");
  }
  openAppTW() {    
    window.open(`https://apps.apple.com/th/app/thai-weather/id734275345`, "_blank");
  }
  openAppTS() {    
    window.open(`https://apps.apple.com/th/app/thai-stat/id1455071307`, "_blank");
  }
  openAppTTP() {    
    window.open(`https://apps.apple.com/th/app/track-trace-thailand-post/id1476524085`, "_blank");
  }
  openAppTPM() {    
    window.open(`https://apps.apple.com/th/app/thailandpostmart-com/id1317225725`, "_blank");
  }
  openAppPP() {    
    window.open(`https://apps.apple.com/th/app/post-portal-%E0%B8%A3%E0%B8%B0%E0%B8%9A%E0%B8%9A%E0%B8%AB%E0%B8%99-%E0%B8%B2%E0%B8%88%E0%B8%AD%E0%B8%AB%E0%B8%A5-%E0%B8%81/id952956000`, "_blank");
  }

  
    
  
}

