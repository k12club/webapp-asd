import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer-style-two',
  templateUrl: './footer-style-two.component.html',
  styleUrls: ['./footer-style-two.component.scss']
})
export class FooterStyleTwoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


  openFacebook() {    
    window.open(`https://m.facebook.com/%E0%B8%AD%E0%B8%B2%E0%B8%AA%E0%B8%B2%E0%B8%AA%E0%B8%A1%E0%B8%B1%E0%B8%84%E0%B8%A3%E0%B8%94%E0%B8%B4%E0%B8%88%E0%B8%B4%E0%B8%97%E0%B8%B1%E0%B8%A5-Thailand-Digital-Volunteers-100451951826191/?ref=bookmarks&mt_nav=0`, "_blank");
  }
  openLineOpenChat() {    
    window.open(`https://line.me/ti/g2/xrwwjSFm91xbNolocPCh5Q?utm_source=invitation&utm_medium=link_copy&utm_campaign=default`, "_blank");
  }

}
