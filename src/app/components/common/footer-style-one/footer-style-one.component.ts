import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer-style-one',
  templateUrl: './footer-style-one.component.html',
  styleUrls: ['./footer-style-one.component.scss']
})
export class FooterStyleOneComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  openFacebook() {    
    window.open(`https://www.facebook.com/TDV.Thailand/`, "_blank");
  }
  openLineOpenChat() {    
    window.open(`https://line.me/ti/g2/xrwwjSFm91xbNolocPCh5Q?utm_source=invitation&utm_medium=link_copy&utm_campaign=default`, "_blank");
  }
}
