import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../services/auth/_services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar-style-one',
  templateUrl: './navbar-style-one.component.html',
  styleUrls: ['./navbar-style-one.component.scss']
})
export class NavbarStyleOneComponent implements OnInit {

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }
  currentUser = this.authenticationService.currentUserValue;


  ngOnInit(): void {
    console.log('currentUser',this.currentUser);
    
  }

  openGen_1() {
    // window.open(`https://drive.google.com/file/d/1lAVA1344Hc34rxfwsM9_fOalDM0PiGIT/view?usp=sharing`, "_blank");
    window.open("/assets/pdf/ข้อมูลทำเนียบรุ่น1.pdf", "_blank");
  }
  Logout() {
    location.href = "/"
    this.authenticationService.logout()
    // this.router.navigate(['/']);
  }
}
