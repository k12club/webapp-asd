import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../auth/_services/authentication.service';

@Component({
  selector: 'app-login-nav-component',
  templateUrl: './login-nav.component.html',
  styleUrls: ['./login-nav.component.scss']
})
export class LoginNavComponent implements OnInit {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }
  currentUser = this.authenticationService.currentUserValue;

  ngOnInit(): void {
  }

  Logout() {
    location.href = "/" 
    this.authenticationService.logout()
  }

}
