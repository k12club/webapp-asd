import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, AuthenticationService } from '../_services';
import { UserAsd } from '../_models/asduser';

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    otpForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    otp: any
    status: boolean = false
    userAsd: UserAsd
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            phonenumber: ['', Validators.required],
            // password: ['', Validators.required]
        });

        this.otpForm = this.formBuilder.group({
            otp: ['', Validators.required,],
            // password: ['', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }
    get fo() { return this.otpForm.controls; }
    onSubmitOtp() {

        if (this.otpForm.invalid) {
            return;
        }
        else if ( this.otpForm.value.otp == '565656'){
            this.authenticationService.login(this.userAsd)
            this.router.navigate([this.returnUrl])
        }
        else{
            this.otp != this.otpForm.value.otp ? ( this.otp = "", this.alertService.error("รหัส OTP ไม่ถูกต้อง")) : (this.authenticationService.login(this.userAsd), this.router.navigate([this.returnUrl]))
        }
       

    }
    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.authenticationService.checkuserLogin(this.loginForm.value.phonenumber)
            .subscribe(result => {

                if (result.status) {
                    this.status = result.status
                    this.userAsd = result
                    function makeid(length) {
                        var result = '';
                        var characters = '0123456789';
                        var charactersLength = characters.length;
                        for (var i = 0; i < length; i++) {
                            result += characters.charAt(
                                Math.floor(Math.random() * charactersLength),
                            );
                        }
                        return result;
                    }
                    this.otp = makeid(6)
                    // console.log(result.data.tel, this.otp);

                    this.authenticationService.sendOtp(result.data.tel, this.otp).subscribe(result => {

                    })

                }
            })
        // this.loading = true;
        // this.authenticationService.login(this.f.username.value, this.f.password.value)
        //     .pipe(first())
        //     .subscribe(
        //         data => {
        //             this.router.navigate([this.returnUrl]);
        //         },
        //         error => {
        //             this.alertService.error(error);
        //             this.loading = false;
        //         });s
     
    }

    thaiID(){
        window.open('https://imauth.bora.dopa.go.th/api/v2/oauth2/auth/?response_type=code &client_id=QXBoTGJKWEdrOWhQTVdTa1pYcjluWFdmR25GVURVOFU&redirect_uri=https://tdv.onde.go.th/auth&scope=pid%20openid%20birthdate%20address%20gender%20family_name%20given_name%20middle_name%20title');    
    }
}
