import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../_models';
import { UserAsd } from '../_models/asduser';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<UserAsd>;
    public currentUser: Observable<UserAsd>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<UserAsd>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): UserAsd {
        return this.currentUserSubject.value;
    }

    // login(username: string, password: string) {
    //     return this.http.post<any>(`/users/authenticate`, { username, password })
    //         .pipe(map(user => {
    //             // login successful if there's a jwt token in the response
    //             if (user && user.token) {
    //                 // store user details and jwt token in local storage to keep user logged in between page refreshes
    //                 localStorage.setItem('currentUser', JSON.stringify(user));
    //                 this.currentUserSubject.next(user);
    //             }

    //             return user;
    //         }));
    // }
    login(userAsd: UserAsd) {
        // 
        
        // login successful if there's a jwt token in the response
        if (userAsd && userAsd.status) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(userAsd));
            this.currentUserSubject.next(userAsd);
        }

        return userAsd;
    }

    logout() {
        // console.log('ออกจากระบบ');

        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    checkuserLogin(tel) {
        return this.http.post<UserAsd>('https://tdv.netpracharat.com:5000/api/checkuser', { tel: tel })
    }

    sendOtp(tel, Randomotp) {
        // console.log(tel, Randomotp);
        // return this.http.post<any>('http://203.113.11.167:3000/smpp', {
        //     phone: tel,
        //     message: 'รหัสยืนยันของคุณคือ : ' + Randomotp,
        //     from: 'ASD'
        // })
        const formData = {
            phone: tel,
            message: 'รหัสยืนยันของคุณคือ : ' + Randomotp,
            // from: 'ASD'
          }
        return this.http.post<any>('https://tdv.netpracharat.com:5000/api/checkOTP',formData)
    }
}