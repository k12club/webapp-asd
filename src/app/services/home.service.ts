import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NewsAsd } from '../models/newsAsd';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  // private apiUrl = 'https://e-training.tpqi.go.th/api/3rdparty/courses';
  private apiUrl = '/api/3rdparty/courses'; // This will be proxied

  constructor(private http: HttpClient) { }

  getNewsAsd() {
    return this.http.get<NewsAsd[]>('https://tdv.netpracharat.com:5000/api/getNews')
  }

  getDetailsNews(id) {
    return this.http.get('https://tdv.netpracharat.com:5000/api/getDetail/' + id)
  }

  getActAsd() {
    return this.http.get<any[]>('https://tdv.netpracharat.com:5000/api/userregister')
  }

  getActAsdDetail(id) {
    return this.http.get('https://tdv.netpracharat.com:5000/api/getDetailActivity/' + id)
  }

  getGeneration1() {
    return this.http.get<any[]>('https://tdv.netpracharat.com:5000/api/usersgen/1')
  }

  getGeneration2(id) {
    return this.http.get<any[]>('https://tdv.netpracharat.com:5000/api/usersgen/' + id)
  }
  getGeneration3() {
    return this.http.get<any[]>('https://tdv.netpracharat.com:5000/api/usersgen/3')
  }
  getGeneration4() {
    return this.http.get<any[]>('https://tdv.netpracharat.com:5000/api/usersgen/4')
  }
  getGeneration5() {
    return this.http.get<any[]>('https://tdv.netpracharat.com:5000/api/usersgen/5')
  }
  getKnowledge() {
    return this.http.get<any>('http://npcrapi-all.netpracharat.com/api/media/all')
  }

  getGeneralNews() {
    return this.http.get<any>('https://tdv.netpracharat.com/api/v1/kao')
  }

  getAsaNews() {
    return this.http.get<any>('https://tdv.netpracharat.com:5000/api/userregister?page=1')
  }

  getProvince() {
    return this.http.get<any[]>("https://tdv.netpracharat.com:5000/api/province");
  }

  getAmphoe(provinceId) {
    return this.http.get("https://tdv.netpracharat.com:5000/api/province/" + provinceId + "/amphoe")
  }

  getSendWork() {
    return this.http.get<any[]>("https://tdv.netpracharat.com:5000/api/getWork")
  }

  getDigitalNews() {
    return this.http.get<any[]>("https://tdv.netpracharat.com:5000/api/getNewsDigital")
  }

  getDigitalUpdate() {
    return this.http.get<any[]>("https://tdv.netpracharat.com:5000/api/PR")
  }
  getDetailsDigitalUpdate(id) {
    return this.http.get("https://tdv.netpracharat.com:5000/api/getDetailPR/" + id)
  }
  getManual() {
    return this.http.get<any[]>("https://tdv.netpracharat.com:5000/api/Manual")
  }
  getCourse() {
    return this.http.get<any[]>("https://tdv.netpracharat.com:5000/api/getCoreSubject")

  }

  getDetailsCourse(id) {
    return this.http.get<any>("https://tdv.netpracharat.com:5000/api/subjectByid/" + id)
  }
  getListTrainingUser(id) {
    const formData = {
      genID: id
    }
    return this.http.post<any[]>("https://tdv.netpracharat.com:5000/api/choose", formData)
  }

  getGeneration6() {
    return this.http.get<any[]>('https://tdv.netpracharat.com:5000/api/usersanothergen/6')
  }
  getGeneration_Another(id) {
    return this.http.get<any[]>('https://tdv.netpracharat.com:5000/api/usersanothergen/' + id)
  }

  getAssessment() {
    return this.http.get<any[]>(`https://tdv.netpracharat.com:5000/api/getQuestion`);
  }
  sendAssessment(data) {
    // console.log(data);

    const formData = {
      id: data.user_id,
      testscore: data.totalScore,
      datetestscore: Date.now()
    }
    return this.http.patch<any[]>("https://tdv.netpracharat.com:5000/api/sendTestScore", formData)
  }

  ThaiIdLogin(code, type) {
    let uri = type == "mobile" ? "https://tdv.onde.go.th/authmobile" : "https://tdv.onde.go.th/auth"
    const formData = {
      grant_type: "authorization_code",
      code: code,
      redirect_uri: uri
    }
    return this.http.post<any>("https://tdv.netpracharat.com/api/v1/thaiID", formData)
  }

  ThaiIdOndeLogin(code) {
    let uri = "https://tdv.onde.go.th/authmobileonde"
    const formData = {
      grant_type: "authorization_code",
      code: code,
      redirect_uri: uri
    }
    return this.http.post<any>("https://tdv.netpracharat.com/api/v1/thaiIDOnde", formData)
  }

  checkThaiID(formData) {

    return this.http.post<any>("https://tdv.netpracharat.com:5000/api/checkThaiID", formData)
  }

  getAssetLinks() {
    return this.http.get('/assets/assetlinks.json');
  }

  reportDashboard(year) {
    return this.http.get<any>("https://tdv.netpracharat.com:5000/api/reportDashboard?year=" + year)
  }

  reportDashboardProvince(province, year) {
    return this.http.get<any>("https://tdv.netpracharat.com:5000/api/reportDashboardProvince?province=" + province + "&year=" + year);
  }

  reportDashboardAmphoe(province, year) {
    return this.http.get<any>("https://tdv.netpracharat.com:5000/api/reportCountAmphoe?province=" + province  + "&year=" + year)
  }

  colorMap(year) {
    return this.http.get<any>("https://tdv.netpracharat.com:5000/api/colorThaiMap?year=" + year)
  }

  getTPQI(): Observable<any> {

    return this.http.get<any>("https://tdv.netpracharat.com/api/3rdparty/courses");
  }
  getDCC(): Observable<any> {
    return this.http.get<any>("https://tdv.netpracharat.com/api/lms/public/course");
  }

  getGCC(): Observable<any> {
    return this.http.get<any>("https://www.gcc.go.th/wp-json/wp/v2/posts",);
  }
}
