import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeOneComponent } from './components/pages/home-one/home-one.component';
import { HomeTwoComponent } from './components/pages/home-two/home-two.component';
import { HomeThreeComponent } from './components/pages/home-three/home-three.component';
import { AboutComponent } from './components/pages/about/about.component';
import { CategoriesComponent } from './components/pages/categories/categories.component';
import { ServicesComponent } from './components/pages/services/services.component';
import { ServicesDetailsComponent } from './components/pages/services-details/services-details.component';
import { BlogComponent } from './components/pages/blog/blog.component';
import { BlogDetailsComponent } from './components/pages/blog-details/blog-details.component';
import { FoodCollectionComponent } from './components/pages/food-collection/food-collection.component';
import { OnlineOrderComponent } from './components/pages/online-order/online-order.component';
import { ChefsComponent } from './components/pages/chefs/chefs.component';
import { BookTableComponent } from './components/pages/book-table/book-table.component';
import { CartComponent } from './components/pages/cart/cart.component';
import { CheckoutComponent } from './components/pages/checkout/checkout.component';
import { ComingSoonComponent } from './components/pages/coming-soon/coming-soon.component';
import { TermsConditionsComponent } from './components/pages/terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './components/pages/privacy-policy/privacy-policy.component';
import { ErrorComponent } from './components/pages/error/error.component';
import { ContactComponent } from './components/pages/contact/contact.component';
import { FaqComponent } from './components/pages/faq/faq.component';
import { NewsGeneralComponent } from './components/pages/news-general/news-general.component'
import { KnowledgeComponent } from './components/pages/knowlegde/knowledge.component'
import { HealtyComponent } from './components/pages/healty/healty.component';
// import { RegisterComponent } from './services/auth/register register';
import { AuthGuard } from './services/auth/_helpers';
import { LoginComponent } from './services/auth/login';
import {NewGeneralDetailsComponent} from './components/pages/new-general-details/new-general-details.component'
import { ElearningComponent } from './components/pages/elearning/elearning.component';
import { ElearingDetailsComponent } from './components/pages/elearning/elearing-details/elearing-details.component';
import {RegulationTrainingComponent} from './components/pages/regulation-training/regulation-training.component'
import { ChecklistTrainingComponent } from './components/pages/checklist-training/checklist-training.component';
import { VillageComponent } from './components/pages/faq/village/village.component';
import { QuestionComponent } from './components/pages/question/question.component';
import { AuthComponent } from './components/pages/auth/auth.component';
import { AuthmobileComponent } from './views/pages/authmobile/authmobile.component';
import { AssetlinksComponent } from './view/pages/assetlinks/assetlinks.component';
import { DashboardComponent } from './views/pages/dashboard/dashboard.component';
import { DigitalComponent } from './views/pages/digital/digital.component';
import { AfncComponent } from './views/pages/afnc/afnc.component';
import { AuthmobileOndeComponent } from './views/pages/authmobileonde/authmobileonde.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: "home" },

    { path: 'home', component: HomeThreeComponent },
    { path: 'home-two', component: HomeTwoComponent },
    { path: 'home-three', component: HomeThreeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'categories', component: CategoriesComponent },
    { path: 'digitalnews', component: ServicesComponent },
    { path: 'services-details/:id', component: ServicesDetailsComponent },
    { path: 'volunteernews', component: BlogComponent },
    { path: 'blog-details/:id', component: BlogDetailsComponent },
    { path: 'food-collection', component: FoodCollectionComponent },
    { path: 'online-order', component: OnlineOrderComponent },
    { path: 'chefs', component: ChefsComponent },
    { path: 'classdirectory', component: FaqComponent, canActivate: [AuthGuard] },
    { path: 'classdirectory-village', component: VillageComponent, canActivate: [AuthGuard] },
    { path: 'tdv-assessment', component: QuestionComponent, canActivate: [AuthGuard]  },
    // { path: 'classdirectory', component: FaqComponent, },
    { path: 'book-table', component: BookTableComponent },
    { path: 'cart', component: CartComponent },
    { path: 'register', component: CheckoutComponent },
    { path: 'privacy-policy', component: ComingSoonComponent },
    { path: 'faq', component: TermsConditionsComponent },
    { path: 'otherservices', component: PrivacyPolicyComponent },
    { path: 'error', component: ErrorComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'news-general', component: NewsGeneralComponent },
    { path: 'news-general-details/:id', component: NewGeneralDetailsComponent},
    { path: 'knowledge', component: KnowledgeComponent },
    { path: 'healty', component: HealtyComponent },
    { path: 'login', component: LoginComponent },
    { path: 'elearning', component: ElearningComponent },
    { path: 'elearning-details/:id', component: ElearingDetailsComponent },
    { path: 'regulation-training', component: RegulationTrainingComponent },
    { path: 'checklist-training', component: ChecklistTrainingComponent },
    { path: 'auth' , component :  AuthComponent},
    { path: 'authmobile' , component :  AuthmobileComponent},
    { path: 'authmobileonde' , component :  AuthmobileOndeComponent},
    { path:'.well-known/assetlinks.json' , component : AssetlinksComponent},
    { path: 'dashboard' , component : DashboardComponent},
    { path: 'digital-community' , component : DigitalComponent},
    { path: 'afnc' , component : AfncComponent},
    { path: '**', component: ErrorComponent },

    // { path: '**', pathMatch: 'full', redirectTo: "home" }

];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
